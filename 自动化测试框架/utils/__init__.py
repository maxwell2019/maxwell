# -*- coding: utf-8 -*-
# @File : __init__.py.py
# @Author : Maxwell
# @Time : 2019/10/14  19:02
# @Site : 
# @Software: PyCharm
# @QQ:7734374
# @Webchat :7734374
# @E-mail :7734374@qq.com
# @来源：TOPTesting软件测试网
'''
||-common    封装常用方法

||——base_page   页面基本操作（如前进、后退、关闭、切换窗口、弹窗处理、元素定位、点击、输入、截图等）

||-file_reader   封装各种数据文件读取方法，实现脚本参数化

||——csv_reader.py  读取csv文件，支持逐行或逐列读取数据

||——excel_reader.py  读取excel文件

||——yaml_reader.py  读取yml文件，配置文件读取就是调用这个类中的get_data()方法

'''