# -*- coding: utf-8 -*-
# @File : __init__.py.py
# @Author : Maxwell
# @Time : 2019/10/14  18:58
# @Site : 
# @Software: PyCharm
# @QQ:7734374
# @Webchat :7734374
# @E-mail :7734374@qq.com
# @来源：TOPTesting软件测试网
'''
||-config.yml  配置文件，主要存放一些全局配置变量，如：

（1）截图、报告、驱动存放路径；

（2）发送邮件使用到的发件人邮箱及登录密码（因为发送邮件一般用于自动发送测试报告，所以发件人比较固定）；

（3）绕过登录验证码要写入的cookies信息

||-config.py   包含读取配置文件方法和一些全局参数

'''