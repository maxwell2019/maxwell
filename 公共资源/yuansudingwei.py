#coding = utf-8
from selenium import webdriver
driver = webdriver.Firefox()
driver.get("http://www.baidu.com")
#通过id定位百度搜索框，并输入内容“selenium”
driver.find_element_by_id("kw").send_keys("selenium")
#通过name定位百度搜索框，并输入内容“selenium”
driver.find_element_by_name("wd").send_keys("selenium")
#通过class_name定位百度搜索框，并输入内容“selenium”
driver.find_element_by_class_name("s_ipt").send_keys("selenium")
#通过tag_name定位百度搜索框，并输入内容“selenium”
driver.find_element_by_tag_name("input").send_keys("selenium")
#通过link_text定位新闻
driver.find_element_by_link_text("新闻").click()
#通过partial_link_text是一种模糊匹配，对于超长字符串截取其中一部分
driver.find_element_by_partial_link_text("新").click()
#通过xpath定位百度搜索框，并输入内容“selenium”
driver.find_element_by_xpath(".//*[@id='kw']").send_keys("selenium")