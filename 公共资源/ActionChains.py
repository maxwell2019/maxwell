#coding=utf-8
from selenium import webdriver
import time
#引入ActionChains类
From selenium.webdriver.common.action_chains import ActionChains
driver = webdriver.Firefox()

#定位到要右击的元素
Right = driver.find_element_by_xpath("xx")
#对定位到的元素执行鼠标右键操作
ActionChains (driver) .context_click(right).perform()
#定位到要双击的元素
double = driver.find_element_by_xpath("xxx")
#对定位到的元素执行鼠标双击操作
ActionChains(driver).double_click(double).perform()
#定位元素的原位置
element = driver.find_element_by_name("xxx")
#定位元素要移动到的目标位置
target = driver.find_element_by_name("xxx")
#执行元素的移动操作
#定位到鼠标移动到上面的元素
above = driver.find_element_by_xpath("xxx")
#对定位到的元素执行鼠标移动到上面的操作
ActionChains(driver).move_to_element(above).perform()
#定位到鼠标按下左键的元素
left = driver.find_element_by_xpath("xxx")
#对定位到的元素执行鼠标左键按下的操作
ActionChains(driver).click_and_hold(left).perform()
