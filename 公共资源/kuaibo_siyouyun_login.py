# coding=utf-8
from selenium import webdriver

driver = webdriver.Firefox()
driver.get("https://pan.baidu.com/")

# 登录
driver.find_element_by_id("user_name").clear()
driver.find_element_by_id("user_name").send_keys("username")
driver.find_element_by_id("user_pwd").clear()
driver.find_element_by_id("user_pwd").send_keys("password")
driver.find_element_by_id("dl_an_submit").click()

# 获得前面title，打印
title = driver.title
print ("title")
# 拿当前URL与预期URL做比较
if title == u"快播私有云":
	print("titleok!")
else:
	print
	"titleon!"
# 获得前面URL，打印
now_url = driver.current_url
print ("now_url")

# 拿当前URL与预期URL做比较
if now_url == "http://webcloud.kuaibo.com/":
	print("urlok!")
else:
	print("urlon!")

# 获得登录成功的用户，打印
now_user = driver.find_element_by_xpath("//div[@id='Nav']/ul/li[4]/a[1]/span").text
print ("now_user")
driver.quit()
