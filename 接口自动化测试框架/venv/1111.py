# -*- coding: utf-8 -*-
# @File : 1111.py
# @Author : Maxwell
# @Time : 2019/10/14  18:27
# @Site : 
# @Software: PyCharm
# @QQ:7734374
# @Webchat :7734374
# @E-mail :7734374@qq.com
# @来源：TOPTesting软件测试网
in params:
try:
    params = data[0]
except KeyError:
    raise KeyError('No.%s Params "%s" not found!' % (self.no, i))
self.params = params


def run(self):
    #   运行用例，暂时只支持POST以及GET方法。client.py中可支持更多参数配置
    h = HTTPsClient(url=self.host + self.url, method=self.requestMethod)
    if self.requestMethod == 'POST':
        self.response = h.json_transform_dict(h.send(data=self.params))
    elif self.requestMethod == 'GET':
        self.response = h.json_transform_dict(h.send(params=self.params))
    else:
        raise KeyError('method "%s" not support!' % self.requestMethod)
    if self.checkPoint:
        try:
            params = self.checkPoint.split('=')
        except TypeError:
            logger.error('checkPoint input error!')
            raise TypeError('checkPoint input error : %s' % self.checkPoint)
        value = str(self.response[params[0]])
        if value != params[1]:
            self.result = 'Fail'
            logger.info('No.%s check point "%s" not found ' % (self.no, self.checkPoint))
            return self.response
        elif value == params[1]:
            self.result = 'Pass'
    else:
        self.result = 'Pass'
    # 判断是否存在关联，如果有且格式正确则将执行后的参数写入配置文件
    #   例如：$token=token,则将response中的token值存入到配置文件中
    if self.relation:
        try:
            l = self.relation.split(',')
        except TypeError:
            logger.error('relation input error!')
            raise TypeError('relation input error : %s' % self.relation)
        for i in l:
            try:
                params = i[1:].split('=')
            except TypeError:
                logger.error('relation input error!')
                raise TypeError('relation input error : %s' % i)
            data = self.y.data[0]
            #   response['data']根据项目不同可能略有差异
            #   此处防止response中想要获取的值为空，产生异常
            if self.response['data'][params[0]] == None: return self.response
            try:
                data[params[1]] = self.response['data'][params[0]].encode('utf-8')
            except AttributeError:
                data[params[1]] = self.response['data'][params[0]]
            # 将变量写入yaml文件
            self.y.data = yaml.dump(data, default_flow_style=False)
    return self.response