# -*- coding: utf-8 -*-
# @File : run.py
# @Author : Maxwell
# @Time : 2019/10/14  17:59
# @Site : 
# @Software: PyCharm
# @QQ:7734374
# @Webchat :7734374
# @E-mail :7734374@qq.com
# @来源：TOPTesting软件测试网
'''
　每次run的时候，会先清空result文件，以便写入新的测试结果
'''
import unittest
def run()：
    Config.creat_file(PATH("../Report/result.xls")
    suite  = unittest.TestSuite.()
#添加测试用例
    suite.addTests(unittest.TestLoader().loadTestsFromTestCase(test_one.TestOne))
    suite.addTests(unittest.TestLoader().loadTestsFromTestCase(test_two.TestTwo))
    runner = unittest.TextTestRunner(verbosity = 2)
    runner.run(suite)
    mail = sendMail.SendMail()
    mail.send

