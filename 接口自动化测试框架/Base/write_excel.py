# -*- coding: utf-8 -*-
# @File : write_excel.py
# @Author : Maxwell
# @Time : 2019/10/14  17:56
# @Site : 
# @Software: PyCharm
# @QQ:7734374
# @Webchat :7734374
# @E-mail :7734374@qq.com
# @来源：TOPTesting软件测试网
from utils.file_reader import YamlReader
from utils.config import BASE_PATH
from utils.client import HTTPsClient
from utils.log import logger
import yaml
import json
import os


class InterfaceCase(object):
    #   接口测试对象，属性对应接口测试xlsx文件。方法有建立用例数据，运行用例数据
    def __init__(self, data):
        self.data = data
        self.no = int('%d' % data['No.'])
        self.apiName = data['API']
        self.testCase = data['TestCase']
        self.host = data['HOST']
        self.url = data['URL']
        self.requestMethod = data['RequestMethod']
        self.params = data['Params']
        self.relation = data['Relation']
        self.checkPoint = data['CheckPoint']
        self.active = data['Active']
        self.result = 'Na'
        self.response = None
        self.y = YamlReader(os.path.join(BASE_PATH, 'config', 'test.yaml'))
        self.get_config()

    def __str__(self):
        #   漂亮的输出测试用例的重要属性
        return 'No.' + str(self.no) + ' API:' + self.apiName + ' TestCase:' + self.testCase + \
          ' result:' + self.result

    def get_config(self):
        #   将参数中带有$的参数与配置文件中的参数匹配，并赋值
        #   例如：参数中有$token，则将配置文件中token的值付给token这个参数
        try:
            params = json.loads(self.params)
        except ValueError:
            logger.warn('No.%s Params input error!' % self.no)
            return
        data = self.y.data

