# -*- coding: utf-8 -*-
# @File : read_excel.py
# @Author : Maxwell
# @Time : 2019/10/14  17:55
# @Site : 
# @Software: PyCharm
# @QQ:7734374
# @Webchat :7734374
# @E-mail :7734374@qq.com
# @来源：TOPTesting软件测试网
from xlrd import open_workbook
from xlutils.copy import copy
from xlwt import Style


class ExcelReader():
    def __init__(self, excel_file, sheet, title_line=True):
        if os.path.exists(excel_file):
            self.excel_file = excel_file
        else:
            raise FileNotFoundError('File not found!%s')
        self.sheet = sheet
        self.title_line = title_line
        self.table = None
        self._data = list()

    def set_value(self, col, row, value):
        #   将传入值写进xlsw文件相应的行号，列号中
        rb = open_workbook(self.excel_file, formatting_info=True)
        wb = copy(rb)
        ws = wb.get_sheet(0)
        ws.write(col, row, value, Style.default_style)
        wb.save(self.excel_file)

    @property
    def data(self):
        #   读取xlsx文件
        if not self._data:
            work_book = open_workbook(self.excel_file)
            if type(self.sheet) not in [int, str]:
                print
                '[Excel sheet error]Input <int> <str> please'
            elif type(self.sheet) is str:
                self.table = work_book.sheet_by_name(self.sheet)
            else:
                self.table = work_book.sheet_by_index(self.sheet)
            # 判断是否有标题，标题有无影响数据的格式(可运行最下方main函数进行比较)
            #   有标题：[{标题1:参数1，标题2：参数2，标题3：参数3}]
            #   无标题：[[参数1(行1)，参数2(行1)，参数3(行1)],[参数1(行2)，参数2(行2)，参数3(行2)]]
            if self.title_line:
                title = self.table.row_values(0)
                for row in range(1, self.table.nrows):
                    self._data.append(dict(zip(title, self.table.row_values(row))))
            else:
                for col in range(self.table.nrows):
                    self._data.append(self.table.row_values(col))
        return self._data