# -*- coding: utf-8 -*-
# @File : No.11 js处理滚动条.py
# @Author : Maxwell
# @Time : 2019/10/11 12:26
# @Site : 
# @Software: PyCharm
# @qq :7734374
# @webchat :7734374
# @E-mail :7734374@qq.com
# @来源：TOPTesting软件测试网
'''
1.api文件将接口划分为:名称,url,请求方法,参数,比对方法,比对字段,预期结果等7个字段,进行接口测试之前需要准备对应的api文件
2.read_excel读取api中的接口,并存储到一个list中.
3.baseRequest中将对read_excel中的list进行请求,并将测试结果传递给write_excel
4.assist用来封装断言方法,比如equle是比较请求结果是否与预期结果一致,contain是返回结果是否包含预期结果,可根据需要添加更多断言
5.write_excel将测试结果写入result文件中
到此处接口请求的基本流程完成.
'''