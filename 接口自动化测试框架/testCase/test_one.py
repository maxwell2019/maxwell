# -*- coding: utf-8 -*-
# @File : test_one.py
# @Author : Maxwell
# @Time : 2019/10/14  17:58
# @Site : 
# @Software: PyCharm
# @QQ:7734374
# @Webchat :7734374
# @E-mail :7734374@qq.com
# @来源：TOPTesting软件测试网
import unittest
import os
from utils.file_reader import ExcelReader
from utils.log import logger
from utils.config import Config, DATA_PATH
from utils.interface_case import InterfaceCase


class InterfaceTest001(unittest.TestCase):
    excel_path = os.path.join(DATA_PATH, Config().get('data')['interface_case_list'])
    e = ExcelReader(excel_path, 0, True)
    case_list = e.data

    def test_func(self):
        for i in self.case_list:
            #   运行用例前，将xlsx文件中执行结果以及用例执行情况还原
            self.e.set_value(int('%d' % i['No.']), 11, 'Na')
            self.e.set_value(int('%d' % i['No.']), 10, '')
            t = InterfaceCase(i)
            #   是否执行该条用例
            if t.active == 'yes':
                response = t.run()
                logger.info(t)
                #   将用例执行结果以及用例执行情况写入xlsx
                self.e.set_value(t.no, 10, response['msg'])
                self.e.set_value(t.no, 11, t.result)


if __name__ == '__main__':
    #   运行测试用例
    unittest.main()