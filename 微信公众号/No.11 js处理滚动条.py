# -*- coding: utf-8 -*-
# @File : No.11 js处理滚动条.py
# @Author : Maxwell
# @Time : 2019/10/11 12:26
# @Site : 
# @Software: PyCharm
# @qq :7734374
# @webchat :7734374
# @E-mail :7734374@qq.com
# @来源：TOPTesting软件测试网

from selenium import webdriver
driver = webdriver.Firefox()
driver.get("https://www.baidu.com")
print (driver.name)
# 回到顶部
def scroll_top():
       if driver.name == "chrome":
              js = "var q=document.body.scrollTop=0"
       else:
               js = "var q=document.documentElement.scrollTop=0"
       return driver.execute_script(js)
#拉到底部
def scroll_foot():
      if driver.name == "chrome":
               js = "var q=document.body.scrollTop=10000"
      else:
               js = "var q=document.documentElement.scrollTop=10000"
      return driver.execute_script(js)

#滚动到底部
js = "window.scrollTo(0,document.body.scrollHeight)"
driver.execute_script(js)

#滚动到顶部
js = "window.scrollTo(0,0)"
driver.execute_script(js)

# 聚焦元素
target = driver.find_element_by_()
driver.execute_script("arguments[0].scrollIntoView();", target)