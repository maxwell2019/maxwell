# coding:utf-8
from selenium import webdriver
import time
driver = webdriver.Firefox()
driver.get("file:///F:/python/mypro/div.html")
# 纵向底部
js1 = 'document.getElementById("TOPTesting").scrollTop = 10000'
driver.execute_script(js1)
time.sleep(5)
# 纵向顶部
js2 = 'document.getElementById("TOPTesting").scrollTop = 0'
driver.execute_script(js2)
# 横向右侧
js3 = 'document.getElementById("TOPTesting").scrollTop = 10000'
driver.execute_script(js3)
time.sleep(5)
# 获取class返回的是list对象，取list第一个
js5 = 'document.getElementsByClassName("scroll")[0].scrollTop = 10000'
driver.execute_script(js5)
time.sleep(5)
# 控制横向滚动条位置
js6 = 'document.getElementsByClassName("scroll")[0].scrollTop = 10000'
driver.execute_script(js6)