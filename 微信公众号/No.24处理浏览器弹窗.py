# encoding:utf-8

from selenium import webdriver
import time

driver = webdriver.Firefox()
driver.get("http://www.runoob.com/try/try.php?filename=tryjs_alert")
driver.switch_to.frame("iframeResult")
driver.find_element_by_xpath("html/body/input").click()
time.sleep(1)
al = driver.switch_to_alert()
time.sleep(1)
al.accept()
#大家能看到，图中的这种弹窗就是现在主流的表现形式，处理这种弹窗可以利用HTML DOM Style 对象，
# 有一个display属性，可以设置元素如何被显示，
driver = webdriver.Firefox()
driver.get("http://sh.xsjedu.org/")
time.sleep(1)
js = 'document.getElementById("doyoo_monitor").style.display="none";'
driver.execute_script(js)

