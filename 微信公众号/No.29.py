# encoding: utf-8
'''
@File: No.29.py
@Time: 2019/10/10 20:13
@Software: PyCharm
@author: maxwell
@qq: 7734374
@webchat:18901233657
@contact: 18901233657@163.com
@license: (C) Copyright 2013-2017, Node Supply Chain Manager Corporation Limited.
'''
import cx_Oracle


# 建立连接
db_connection = cx_Oracle.connect('sys', 'oracle', 'test', cx_Oracle.SYSDBA)
# 使用游标访问数据
db_cursor = db_connection.cursor()
db_cursor.execute("""
    select employee_id, first_name, last_name from hr.employees
    where employee_id > :eid""",
    eid = 200)
# 获取所有数据
print(db_cursor.fetchall())
import
# 关闭游标
db_cursor.close()