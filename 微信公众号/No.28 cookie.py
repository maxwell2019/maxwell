# coding:utf-8
from selenium import webdriver
import time
from selenium.webdriver.support.wait import WebDriverWait

driver = webdriver.Firefox()
# 启动浏览器后获取cookies
print(driver.get_cookies())
driver.get("http://www.cnblogs.com/TOPTesting")
# 打开主页后获取cookies
print (driver.get_cookies())
# 登录后获取cookies
url = "https://passport.cnblogs.com/user/signin"
driver.get(url)
driver.implicitly_wait(30)
# 初始化
def init():
    # 定义为全局变量，方便其他模块使用
    global url, browser, username, password, wait
    # 登录界面的url
    url = 'https://passport.bilibili.com/login'
    # 实例化一个chrome浏览器
    browser = webdriver.Chrome()
    # 用户名
    username = '***********'
    # 密码
    password = '***********'
    # 设置等待超时
    wait = WebDriverWait(browser, 20)
# 登录
def login():
    # 打开登录页面
    browser.get(url)
    # 获取用户名输入框
    user = wait.until(EC.presence_of_element_located((By.ID, 'login-username')))
    # 获取密码输入框
    passwd = wait.until(EC.presence_of_element_located((By.ID, 'login-passwd')))
    # 输入用户名
    user.send_keys(username)
    # 输入密码
    passwd.send_keys(password)
driver.find_element_by_id("LoginName").send_keys(u"TOPTesting")
driver.find_element_by_id("Password").send_keys(u"Jiahua031^")
driver.find_element_by_id("submitBtn").click()
time.sleep(3)
print (driver.get_cookies())

# 获取指定name的cookie
print (driver.get_cookie(name=".CNBlogsCookie"))

# 清除指定name的cookie
driver.delete_cookie(name=".CNBlogsCookie")
print (driver.get_cookies())
# 为了验证此cookie是登录的，可以删除后刷新页面
driver.refresh()

# 清除所有的cookie
driver.delete_all_cookies()
print (driver.get_cookies())
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By


