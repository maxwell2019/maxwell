#coding=utf-8
from selenium import webdriver
import time
driver = webdriver.Firefox()
driver.get("https://github.com/login")
driver.maximize_window()
#time.sleep(3)
driver.implicitly_wait(10)
time.sleep(3)
driver.find_element_by_id("login_field").clear()
driver.find_element_by_id("login_field").send_keys("yourusername2008")
time.sleep(3)
driver.find_element_by_id("password").clear()
driver.find_element_by_id("password").send_keys("yourpws2008")
time.sleep(3)
driver.find_element_by_xpath(".//*[@id='login']/form/div[3]/input[7]").click()
# 登录成功后，获取我的账户名称
time.sleep(5)
# 点右上角设置
driver.find_element_by_xpath("html/body/div[1]/header/div[7]/details/summary/img").click()
driver.find_element_by_xpath("html/body/div[1]/header/div[7]/details/details-menu/div[1]/a/strong").click()
# 获取账户名称
time.sleep(1)
t = driver.find_element_by_css_selector(".p-name.vcard-fullname.d-block.overflow-hidden").text
print("获取到我的账户名称：%s" % t)

if t == "Maxwell":
    print("登录成功！")
else:
    print("登录失败！")
#点sign out退出登录
driver.find_element_by_xpath("html/body/div[1]/header/div[7]/details/summary/img").click()
driver.find_element_by_css_selector(".dropdown-item.dropdown-signout").click()
driver.quit()

# coding:utf-8
from selenium import webdriver
import time

def login(driver, user, password):
    '''登录github'''
    # 打开github首页
    driver.get("https://github.com/login")
    driver.implicitly_wait(10)
    # 输入账号
    driver.find_element_by_id("login_field").send_keys(user)
    # 输入密码
    driver.find_element_by_id("password").send_keys(password)
    driver.find_element_by_name("commit").click()

def logout(driver):
    '''退出github'''
    time.sleep(3)
    # 点右上角设置
    driver.find_element_by_css_selector(".HeaderNavlink.name.mt-1").click()
    time.sleep(1)
    # 点sign out
    driver.find_element_by_css_selector(".dropdown-item.dropdown-signout").click()
    driver.quit()

if __name__ == "__main__":
    driver = webdriver.Firefox()
    # 调用登录
    login(driver, "youruser", "yourpsw")
    print("hello  yoyo!")
    # 调用退出
    logout(driver)

