#coding = utf-8
from selenium import webdriver
driver = webdriver.Firefox()
driver.get("http://www.baidu.com")
#css通过id属性定位
driver.find_element_by_css_selector("#kw").send_keys("python")
#css通过class属性定位
driver.find_element_by_css_selector(".s_ipt").send_keys("selenium 2.0")
#css通过标签属性定位，先来熟悉一下这个写法，此处由于相同标签比较多，肯定会报错的
driver.find_element_by_css_selector("input").send_keys("selenium 3.0")
#css通过name属性定位
driver.find_element_by_css_selector("[name='wd']").send_keys("python2")
#css通过autocomplete属性定位
driver.find_element_by_css_selector("[autocomplete='of']").send_keys("selenium 2.0")
#css通过type属性定位
driver.find_element_by_css_selector("[type='text']").send_keys("python3")
#css通过标签与class属性的组合定位
driver.find_element_by_css_selector("input.s_ipt").send_keys("python3")
#css通过标签与class属性的组合定位
driver.find_element_by_css_selector("input#kw").send_keys("python3")
#css通过标签与id属性的组合定位
driver.find_element_by_css_selector("input.s_ipt").send_keys("python3")
#css通过标签与其他属性的组合定位
driver.find_element_by_css_selector("input[id='kw']").send_keys("python3")
#css通过层级关系定位
driver.find_element_by_css_selector("form#form>span>input").send_keys("python3")
#css通过层级关系定位
driver.find_element_by_css_selector("form.fm>span>input").send_keys("python3")
#选择第一个option
driver.find_element_by_css_selector("select#nr>option:nth-child(1)").click()
#选择第二个option
driver.find_element_by_css_selector("select#nr>option:nth-child(2)").click()
#选择第三个option
driver.find_element_by_css_selector("select#nr>option:nth-child(3)").click()
#css逻辑运算，不需要使用and
driver.find_element_by_css_selector("input[id='kw'][name='wd']").click()
