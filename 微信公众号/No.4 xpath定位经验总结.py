'''
xpath ​XPath为XML路径语言（XML Path Language），它是一种用来确定XML文档中某部分位置的语言，
基于XML的树状结构，提供在数据结构树中找寻节点的能力​，通俗讲就是通过元素的属性路径来定位元素（对象）的坐标。
'''
#coding = utf-8
from selenium import webdriver
driver = webdriver.Firefox()
driver.get("http://www.baidu.com")
#xpath 通过id属性定位
driver.find_element_by_xpath(".//*[@id='kw']").send_keys("selenium")
#xpath 通过name属性定位
driver.find_element_by_xpath(".//*[@name='wd']").send_keys("selenium 2.0")
#xpath 通过class属性定位
driver.find_element_by_xpath(".//*[@class='s_ipt']").send_keys("selenium 3.0")
#xpath 通过其他属性定位
driver.find_element_by_xpath(".//*[@autocomplete='off']").send_keys("selenium 2.0&selenium 3.0")
#xpath 通过id属性定位+标签"input"
driver.find_element_by_xpath(".//input[@id='kw']").send_keys("selenium")
#xpath 通过name属性定位+标签"input"
driver.find_element_by_xpath(".//input[@name='wd']").send_keys("selenium 2.0")
#xpath 通过class属性定位+标签"input"
driver.find_element_by_xpath(".//input[@class='s_ipt']").send_keys("selenium 3.0")
driver.find_element_by_xpath(".//input[@autocomplete='off']").send_keys("selenium 2.0&selenium 3.0")

#使用xpath定位第一个同级别元素
driver.find_element_by_xpath("//select[@id='nr']/option[1]").click()
#使用xpath定位第二个同级别元素
driver.find_element_by_xpath("//select[@id='nr']/option[2]").click()
#使用xpath定位第三个同级别元素
driver.find_element_by_xpath("//select[@id='nr']/option[3]").click()
driver.find_element_by_xpath(".//*[@id='kw' and autocomplete='off']").click()

#使用xpath模糊匹配功能
driver.find_element_by_xpath("//*[contains(text(),'hao123')]").click()
#使用xpath可以模糊匹配某个属性
driver.find_element_by_xpath("//*[contains(@id,'kw')]").click()
#xpath可以模糊匹配以什么开头
driver.find_element_by_xpath("//*[starts-with(@id,'s_kw_')]").click()
#xpath可以模糊匹配以什么结尾
driver.find_element_by_xpath("//*[ends-with(@id,'kw_wrap')]").click()
#xpath支持最强的正则表达式
driver.find_element_by_xpath("//*[matchs(text(),'hao123')]").click()
