#coding = utf-8
#导入webdriver模块
from selenium import webdriver
#导入时间模块
import time
# 打开浏览器
driver = webdriver.Firefox()
#driver = webdriver.Iefox()  #IE浏览器
#driver = webdriver.Chrome() #Chrome浏览器
driver.get("http://baidu.com")
#设置休眠时间为3秒，也可以是小数，单位是秒
time.sleep(3)
#等待3秒后刷新页面
driver.refresh()
#设置浏览器大小500*900
driver.set_window_size(500,900)
time.sleep(2)
#将浏览器最大化显示
print ("浏览器最大化")
driver.maximize_window()
#访问百度首页
first_url= 'http://www.baidu.com'
print("now access %s" %(first_url))
driver.get(first_url)
time.sleep(3)
#访问新闻页面
second_url='http://news.baidu.com'
print ("now access %s" %(second_url))
driver.get(second_url)
time.sleep(3)
#返回（后退）到百度首页
print ("back to %s "%(first_url))
driver.back()
time.sleep(3)
#前进到新闻页
print("forward to %s"%(second_url))
driver.forward()
time.sleep(3)
#截屏后设置制定的保存路径+文件名称+后缀
driver.get_screenshot_as_file("F:\\python\\shot\\baidunews11.png")