*** Settings ***
Library           RequestsLibrary
Library           DatabaseLibrary
Library           Collections
Library           recordLoanData.py
Library           WBLibrary.py
Library           SudsLibrary

*** Variables ***

*** Test Cases ***
Data-至尊宝
    ${ExcelFile}    set variable    temp10.xlsx
    createExcel    ${ExcelFile}
    #>>>>>>>>>>>>>>>    设置数据创建组数
    ${runDataTimes}    set variable    10
    #    创建参数名称
    a-ToExcel-ParaNames    ${ExcelFile}
    #    生成参数值
    : FOR    ${i}    IN RANGE    ${runDataTimes}
    \    ${temp}    ${null}    fromSort    ${ExcelFile}    ${i+2}    1
    \    ...    0
    \    ${temp}    ${null}    retCode    ${ExcelFile}    ${i+2}    2
    \    ...    0000
    \    ${temp}    ${null}    retMsg    ${ExcelFile}    ${i+2}    3
    \    ...    返回信息
    \    ${temp}    ${null}    sourceClient    ${ExcelFile}    ${i+2}    4
    \    ...    1056
    \    ${temp}    ${null}    toSort    ${ExcelFile}    ${i+2}    5
    \    ...    None
    \    ${temp}    ${null}    transExeDate    ${ExcelFile}    ${i+2}    6
    \    ...    None
    \    ${temp}    ${null}    transExeTime    ${ExcelFile}    ${i+2}    7
    \    ...    None
    \    ${temp}    ${null}    transSerialNo    ${ExcelFile}    ${i+2}    8
    \    ...    None
    \    ${customerName}    ${null}    customerName    ${ExcelFile}    ${i+2}    9
    \    ${temp}    ${null}    degree    ${ExcelFile}    ${i+2}    10
    \    ${temp}    ${null}    certType    ${ExcelFile}    ${i+2}    11
    \    ${certId}    ${null}    certId    ${ExcelFile}    ${i+2}    12
    \    ${temp}    ${null}    phone    ${ExcelFile}    ${i+2}    13
    \    ${temp}    ${null}    email    ${ExcelFile}    ${i+2}    14
    \    ${temp}    ${null}    loanTerm    ${ExcelFile}    ${i+2}    15
    \    ${temp}    ${null}    approveSuggestAmt    ${ExcelFile}    ${i+2}    16
    \    ...    100    1000    2
    \    ${temp}    ${null}    riskGrade    ${ExcelFile}    ${i+2}    17
    \    ${temp}    ${null}    score    ${ExcelFile}    ${i+2}    18
    \    ...    1    100
    \    ${temp}    ${null}    productId    ${ExcelFile}    ${i+2}    19
    \    ...    338
    \    ${temp}    ${null}    oldAppId    ${ExcelFile}    ${i+2}    20
    \    ${temp}    ${null}    borrowerType    ${ExcelFile}    ${i+2}    21
    \    ...    B154001
    \    ${temp}    ${null}    instCode    ${ExcelFile}    ${i+2}    22
    \    ...    110857
    \    ${temp}    ${null}    saleChannel    ${ExcelFile}    ${i+2}    23
    \    ...    1056
    \    ${temp}    ${null}    career    ${ExcelFile}    ${i+2}    24
    \    ${temp}    ${null}    liveAddress    ${ExcelFile}    ${i+2}    25
    \    ${temp}    ${null}    marry    ${ExcelFile}    ${i+2}    26
    \    ${temp}    ${null}    loanPurpose    ${ExcelFile}    ${i+2}    27
    \    ${uumCustNo}    ${uumUserId}    a-CustNo+UserId    ${customerName}    ${certId}
    \    ${temp}    ${null}    key1    ${ExcelFile}    ${i+2}    28
    \    ${temp}    ${null}    value1    ${ExcelFile}    ${i+2}    29
    \    ...    ${uumCustNo}
    \    ${temp}    ${null}    key2    ${ExcelFile}    ${i+2}    30
    \    ${temp}    ${null}    value2    ${ExcelFile}    ${i+2}    31
    \    ...    ${uumUserId}
    \    ${temp}    ${null}    modelId    ${ExcelFile}    ${i+2}    32
    \    ...    26
    \    ${accBankName}    ${null}    accBankName    ${ExcelFile}    ${i+2}    33
    \    ${temp}    ${null}    accBankBranch    ${ExcelFile}    ${i+2}    34
    \    ...    ${accBankName}
    \    ${temp}    ${null}    accProvince    ${ExcelFile}    ${i+2}    35
    \    ${temp}    ${null}    accCity    ${ExcelFile}    ${i+2}    36
    \    ${temp}    ${null}    accBankCard    ${ExcelFile}    ${i+2}    37
    \    ...    ${accBankName}
    \    ${temp}    ${null}    accOwnName    ${ExcelFile}    ${i+2}    38
    \    ...    ${customerName}
    \    ${temp}    ${null}    accOwnPhone    ${ExcelFile}    ${i+2}    39
    \    ${temp}    ${null}    accCertType    ${ExcelFile}    ${i+2}    40
    \    ${temp}    ${null}    accOwnIdCard    ${ExcelFile}    ${i+2}    41
    \    ...    ${certId}
    \    ${temp}    ${null}    accountType    ${ExcelFile}    ${i+2}    42
    \    ...    01
    \    ${temp}    ${null}    accType    ${ExcelFile}    ${i+2}    43
    \    ...    B6802
    \    ${temp}    ${null}    trusteeType    ${ExcelFile}    ${i+2}    44
    \    ...    B134003
    \    ${temp}    ${null}    accBankCardBindId    ${ExcelFile}    ${i+2}    45
    \    ...    2
    \    ${temp}    ${null}    accBankName    ${ExcelFile}    ${i+2}    46
    \    ${temp}    ${null}    accBankBranch    ${ExcelFile}    ${i+2}    47
    \    ...    ${accBankName}
    \    ${temp}    ${null}    accProvince    ${ExcelFile}    ${i+2}    48
    \    ${temp}    ${null}    accCity    ${ExcelFile}    ${i+2}    49
    \    ${temp}    ${null}    accBankCard    ${ExcelFile}    ${i+2}    50
    \    ...    ${accBankName}
    \    ${temp}    ${null}    accOwnName    ${ExcelFile}    ${i+2}    51
    \    ...    ${customerName}
    \    ${temp}    ${null}    accOwnPhone    ${ExcelFile}    ${i+2}    52
    \    ${temp}    ${null}    accCertType    ${ExcelFile}    ${i+2}    53
    \    ${temp}    ${null}    accOwnIdCard    ${ExcelFile}    ${i+2}    54
    \    ...    ${certId}
    \    ${temp}    ${null}    accountType    ${ExcelFile}    ${i+2}    55
    \    ...    01
    \    ${temp}    ${null}    accType    ${ExcelFile}    ${i+2}    56
    \    ...    B6801
    \    ${temp}    ${null}    trusteeType    ${ExcelFile}    ${i+2}    57
    \    ...    B134003
    \    ${temp}    ${null}    accBankCardBindId    ${ExcelFile}    ${i+2}    58
    \    ...    1
    \    ${temp}    ${null}    orgnCreditCode    ${ExcelFile}    ${i+2}    59
    \    ...    12563

RL-至尊宝
    ###
    ###说明-1：    #>>>>>>>>>>>>>>>    需手动修改
    ###说明-2：    #    脚本注释说明
    ###
    #>>>>>>>>>>>>>>>    指定参数输入文档    !!!xlsx格式
    ${ExcelFile}    set variable    temp10.xlsx
    #>>>>>>>>>>>>>>>    指定参数输入文档Sheet名
    ${SheetName}    set variable    Sheet
    #>>>>>>>>>>>>>>>    设置WebService服务器地址
    ${url}    set variable    http://123.57.48.237:7082/webservice/loanService?wsdl
    #    初始化变量名
    @{paraNamesList}    Create list
    @{paraNamesList}    a-INIT-ParaNames    ${ExcelFile}    Sheet
    @{paraValuesList}    Create list
    #    创建服务器连接
    ${client}    CreateClient    ${url}
    Create Soap Client    ${url}
    #>>>>>>>>>>>>>>>    指定运行次数
    ${runTimes}    set variable    10
    #    循环取参和取值
    : FOR    ${i}    IN RANGE    ${runTimes}
    \    ${head}    Create Wsdl Object    ns0:TransHead
    \    ${body}    Create Wsdl Object    ns0:lonApp
    \    ${tLonAccount1}    Create Wsdl Object    ns0:tLonAccount
    \    ${entry}    Create Wsdl Object    ns0:entry
    \    ${entry1}    Create Wsdl Object    ns0:entry
    \    ${modelBusinessData}    Create Wsdl Object    ns0:commonObject
    \    ${propertyMap}    Create Wsdl Object    ns0:propertyMap
    \    ${tLonAccount}    Create Wsdl Object    ns0:tLonAccount
    \    @{paraValuesList}    a-INIT-ParaValues    ${ExcelFile}    ${SheetName}    ${i+1}
    \    Set Wsdl Object Attribute    ${head}    @{paraNamesList}[0]    @{paraValuesList}[0]
    \    Set Wsdl Object Attribute    ${head}    @{paraNamesList}[1]    @{paraValuesList}[1]
    \    Set Wsdl Object Attribute    ${head}    @{paraNamesList}[2]    @{paraValuesList}[2]
    \    Set Wsdl Object Attribute    ${head}    @{paraNamesList}[3]    @{paraValuesList}[3]
    \    Set Wsdl Object Attribute    ${head}    @{paraNamesList}[4]     ${None}
    \    Set Wsdl Object Attribute    ${head}    @{paraNamesList}[5]     ${None}
    \    Set Wsdl Object Attribute    ${head}    @{paraNamesList}[6]     ${None}
    \    Set Wsdl Object Attribute    ${head}    @{paraNamesList}[7]     ${None}
    \    Set Wsdl Object Attribute    ${body}    @{paraNamesList}[8]    @{paraValuesList}[8]
    \    Set Wsdl Object Attribute    ${body}    @{paraNamesList}[9]    @{paraValuesList}[9]
    \    Set Wsdl Object Attribute    ${body}    @{paraNamesList}[10]    @{paraValuesList}[10]
    \    Set Wsdl Object Attribute    ${body}    @{paraNamesList}[11]    @{paraValuesList}[11]
    \    Set Wsdl Object Attribute    ${body}    @{paraNamesList}[12]    @{paraValuesList}[12]
    \    Set Wsdl Object Attribute    ${body}    @{paraNamesList}[13]    @{paraValuesList}[13]
    \    Set Wsdl Object Attribute    ${body}    @{paraNamesList}[14]    @{paraValuesList}[14]
    \    Set Wsdl Object Attribute    ${body}    @{paraNamesList}[15]    @{paraValuesList}[15]
    \    Set Wsdl Object Attribute    ${body}    @{paraNamesList}[16]    @{paraValuesList}[16]
    \    Set Wsdl Object Attribute    ${body}    @{paraNamesList}[17]    @{paraValuesList}[17]
    \    Set Wsdl Object Attribute    ${body}    @{paraNamesList}[18]    @{paraValuesList}[18]
    \    Set Wsdl Object Attribute    ${body}    @{paraNamesList}[19]    @{paraValuesList}[19]
    \    Set Wsdl Object Attribute    ${body}    @{paraNamesList}[20]    @{paraValuesList}[20]
    \    Set Wsdl Object Attribute    ${body}    @{paraNamesList}[21]    @{paraValuesList}[21]
    \    Set Wsdl Object Attribute    ${body}    @{paraNamesList}[22]    @{paraValuesList}[22]
    \    Set Wsdl Object Attribute    ${body}    @{paraNamesList}[23]    @{paraValuesList}[23]
    \    Set Wsdl Object Attribute    ${body}    @{paraNamesList}[24]    @{paraValuesList}[24]
    \    Set Wsdl Object Attribute    ${body}    @{paraNamesList}[25]    @{paraValuesList}[25]
    \    Set Wsdl Object Attribute    ${body}    @{paraNamesList}[26]    @{paraValuesList}[26]
    \    Set Wsdl Object Attribute    ${entry}    @{paraNamesList}[27]    @{paraValuesList}[27]
    \    Set Wsdl Object Attribute    ${entry}    @{paraNamesList}[28]    @{paraValuesList}[28]
    \    Set Wsdl Object Attribute    ${entry1}    @{paraNamesList}[29]    @{paraValuesList}[29]
    \    Set Wsdl Object Attribute    ${entry1}    @{paraNamesList}[30]    @{paraValuesList}[30]
    \    Set Wsdl Object Attribute    ${modelBusinessData}    @{paraNamesList}[31]    @{paraValuesList}[31]
    \    Set Wsdl Object Attribute    ${tLonAccount}    @{paraNamesList}[32]    @{paraValuesList}[32]
    \    Set Wsdl Object Attribute    ${tLonAccount}    @{paraNamesList}[33]    @{paraValuesList}[33]
    \    Set Wsdl Object Attribute    ${tLonAccount}    @{paraNamesList}[34]    @{paraValuesList}[34]
    \    Set Wsdl Object Attribute    ${tLonAccount}    @{paraNamesList}[35]    @{paraValuesList}[35]
    \    Set Wsdl Object Attribute    ${tLonAccount}    @{paraNamesList}[36]    @{paraValuesList}[36]
    \    Set Wsdl Object Attribute    ${tLonAccount}    @{paraNamesList}[37]    @{paraValuesList}[37]
    \    Set Wsdl Object Attribute    ${tLonAccount}    @{paraNamesList}[38]    @{paraValuesList}[38]
    \    Set Wsdl Object Attribute    ${tLonAccount}    @{paraNamesList}[39]    @{paraValuesList}[39]
    \    Set Wsdl Object Attribute    ${tLonAccount}    @{paraNamesList}[40]    @{paraValuesList}[40]
    \    Set Wsdl Object Attribute    ${tLonAccount}    @{paraNamesList}[41]    @{paraValuesList}[41]
    \    Set Wsdl Object Attribute    ${tLonAccount}    @{paraNamesList}[42]    @{paraValuesList}[42]
    \    Set Wsdl Object Attribute    ${tLonAccount}    @{paraNamesList}[43]    @{paraValuesList}[43]
    \    Set Wsdl Object Attribute    ${tLonAccount}    @{paraNamesList}[44]    @{paraValuesList}[44]
    \    Set Wsdl Object Attribute    ${tLonAccount1}    @{paraNamesList}[45]    @{paraValuesList}[45]
    \    Set Wsdl Object Attribute    ${tLonAccount1}    @{paraNamesList}[46]    @{paraValuesList}[46]
    \    Set Wsdl Object Attribute    ${tLonAccount1}    @{paraNamesList}[47]    @{paraValuesList}[47]
    \    Set Wsdl Object Attribute    ${tLonAccount1}    @{paraNamesList}[48]    @{paraValuesList}[48]
    \    Set Wsdl Object Attribute    ${tLonAccount1}    @{paraNamesList}[49]    @{paraValuesList}[49]
    \    Set Wsdl Object Attribute    ${tLonAccount1}    @{paraNamesList}[50]    @{paraValuesList}[50]
    \    Set Wsdl Object Attribute    ${tLonAccount1}    @{paraNamesList}[51]    @{paraValuesList}[51]
    \    Set Wsdl Object Attribute    ${tLonAccount1}    @{paraNamesList}[52]    @{paraValuesList}[52]
    \    Set Wsdl Object Attribute    ${tLonAccount1}    @{paraNamesList}[53]    @{paraValuesList}[53]
    \    Set Wsdl Object Attribute    ${tLonAccount1}    @{paraNamesList}[54]    @{paraValuesList}[54]
    \    Set Wsdl Object Attribute    ${tLonAccount1}    @{paraNamesList}[55]    @{paraValuesList}[55]
    \    Set Wsdl Object Attribute    ${tLonAccount1}    @{paraNamesList}[56]    @{paraValuesList}[56]
    \    Set Wsdl Object Attribute    ${tLonAccount1}    @{paraNamesList}[57]    @{paraValuesList}[57]
    \    Set Wsdl Object Attribute    ${tLonAccount1}    @{paraNamesList}[58]    @{paraValuesList}[58]
    \    #    非参数赋值
    \    Set Wsdl Object Attribute    ${modelBusinessData}    propertyMap    ${propertyMap}
    \    #    各类关联对象
    \    ${propertyMap}    AddObjectToObject    ${propertyMap}    entry    ${entry}
    \    ${propertyMap}    AddObjectToObject    ${propertyMap}    entry    ${entry1}
    \    #    组装body
    \    ${body}    AddObjectToObject    ${body}    modelBusinessData    ${modelBusinessData}
    \    ${body}    AddObjectToObject    ${body}    tLonAccountList    ${tLonAccount}
    \    ${body}    AddObjectToObject    ${body}    tLonAccountList    ${tLonAccount1}
    \    #    调用服务端方法
    \    Run Keyword And Continue On Failure    a-CallMethods    ${head}    ${body}    ${ExcelFile}    ${i+2}
    \    ...    60

*** Keywords ***
a-INIT-ParaNames
    [Arguments]    ${ExcelFile}    ${sheet}
    ${paraNamesList}    Create list
    ${ColumnCount}    getExcelColumnCount    ${ExcelFile}    ${sheet}
    : FOR    ${i}    IN RANGE    ${ColumnCount}
    \    ${paraNames}    readExcel    ${ExcelFile}    1    ${i+1}
    \    Insert Into List    ${paraNamesList}    ${i}    ${paraNames}
    [Return]    ${paraNamesList}

a-INIT-ParaValues
    [Arguments]    ${ExcelFile}    ${sheet}    ${ValueGet}
    ${paraValuesList}    Create list
    ${ColumnCount}    getExcelColumnCount    ${ExcelFile}    ${sheet}
    ${RowCount}    getExcelRowCount    ${ExcelFile}    ${sheet}
    ${ValueGet}    plusPlus    ${ValueGet}
    : FOR    ${i}    IN RANGE    ${ColumnCount}
    \    ${paraValues}    readExcel    ${ExcelFile}    ${ValueGet}    ${i+1}
    \    Insert Into List    ${paraValuesList}    ${i}    ${paraValues}
    [Return]    ${paraValuesList}

a-ToExcel-ParaNames
    [Arguments]    ${ExcelFile}
    ################    创建参数名称
    addExcel    ${ExcelFile}    1    1    fromSort
    addExcel    ${ExcelFile}    1    2    retCode
    addExcel    ${ExcelFile}    1    3    retMsg
    addExcel    ${ExcelFile}    1    4    sourceClient
    addExcel    ${ExcelFile}    1    5    toSort
    addExcel    ${ExcelFile}    1    6    transExeDate
    addExcel    ${ExcelFile}    1    7    transExeTime
    addExcel    ${ExcelFile}    1    8    transSerialNo
    addExcel    ${ExcelFile}    1    9    customerName
    addExcel    ${ExcelFile}    1    10    degree
    addExcel    ${ExcelFile}    1    11    certType
    addExcel    ${ExcelFile}    1    12    certId
    addExcel    ${ExcelFile}    1    13    phone
    addExcel    ${ExcelFile}    1    14    email
    addExcel    ${ExcelFile}    1    15    loanTerm
    addExcel    ${ExcelFile}    1    16    approveSuggestAmt
    addExcel    ${ExcelFile}    1    17    riskGrade
    addExcel    ${ExcelFile}    1    18    score
    addExcel    ${ExcelFile}    1    19    productId
    addExcel    ${ExcelFile}    1    20    oldAppId
    addExcel    ${ExcelFile}    1    21    borrowerType
    addExcel    ${ExcelFile}    1    22    instCode
    addExcel    ${ExcelFile}    1    23    saleChannel
    addExcel    ${ExcelFile}    1    24    career
    addExcel    ${ExcelFile}    1    25    liveAddress
    addExcel    ${ExcelFile}    1    26    marry
    addExcel    ${ExcelFile}    1    27    loanPurpose
    addExcel    ${ExcelFile}    1    28    key
    addExcel    ${ExcelFile}    1    29    value
    addExcel    ${ExcelFile}    1    30    key
    addExcel    ${ExcelFile}    1    31    value
    addExcel    ${ExcelFile}    1    32    modelId
    addExcel    ${ExcelFile}    1    33    accBankName
    addExcel    ${ExcelFile}    1    34    accBankBranch
    addExcel    ${ExcelFile}    1    35    accProvince
    addExcel    ${ExcelFile}    1    36    accCity
    addExcel    ${ExcelFile}    1    37    accBankCard
    addExcel    ${ExcelFile}    1    38    accOwnName
    addExcel    ${ExcelFile}    1    39    accOwnPhone
    addExcel    ${ExcelFile}    1    40    accCertType
    addExcel    ${ExcelFile}    1    41    accOwnIdCard
    addExcel    ${ExcelFile}    1    42    accountType
    addExcel    ${ExcelFile}    1    43    accType
    addExcel    ${ExcelFile}    1    44    trusteeType
    addExcel    ${ExcelFile}    1    45    accBankCardBindId
    addExcel    ${ExcelFile}    1    46    accBankName
    addExcel    ${ExcelFile}    1    47    accBankBranch
    addExcel    ${ExcelFile}    1    48    accProvince
    addExcel    ${ExcelFile}    1    49    accCity
    addExcel    ${ExcelFile}    1    50    accBankCard
    addExcel    ${ExcelFile}    1    51    accOwnName
    addExcel    ${ExcelFile}    1    52    accOwnPhone
    addExcel    ${ExcelFile}    1    53    accCertType
    addExcel    ${ExcelFile}    1    54    accOwnIdCard
    addExcel    ${ExcelFile}    1    55    accountType
    addExcel    ${ExcelFile}    1    56    accType
    addExcel    ${ExcelFile}    1    57    trusteeType
    addExcel    ${ExcelFile}    1    58    accBankCardBindId
    addExcel    ${ExcelFile}    1    59    orgnCreditCode
    addExcel    ${ExcelFile}    1    60    appId

a-CustNo+UserId
    [Arguments]    ${customerName}    ${certId}
    ${url}    set variable    http://101.200.87.116:9190
    Create Session    api    ${url}
    ${realName}    set variable    ${customerName}
    ${idCardNo}    set variable    ${certId}
    ${addr}    get request    api    /usercenter/new/realNameRegistForBus.do?jSON={"body":{"key":"d20f83ee6933aa1ea047fe5cbd9c1fd5","realName":"${realName}","idCardNo":"${idCardNo}","password":"123456","domain":"bus","type":"1"}}
    ${respjson}    To Json    ${addr.content}
    ${uumCustNo}    get from dictionary    ${respjson}    custNo
    ${uumUserId}    get from dictionary    ${respjson}    uuid
    [Return]    ${uumCustNo}    ${uumUserId}

a-CallMethods
    [Arguments]    ${head}    ${body}    ${ExcelFile}    ${Row}    ${Column}
    ${res}    Call Soap Method    recordLoan    ${head}    ${body}
    ${restransBody}    get from dictionary    ${res}    transBody
    ${resentity}    get from dictionary    ${restransBody}    entity
    ${resappId}    set variable    ${resentity[0][0]}
    addExcel    ${ExcelFile}    ${Row}     ${Column}    ${resappId}

fromSort
    [Arguments]    ${ExcelFile}    ${Row}    ${Column}    ${input}
    #所属机构
    ${fromSort}    set variable    ${input}
    addExcel    ${ExcelFile}    ${Row}    ${Column}    ${fromSort}
    [Return]    ${fromSort}    ${null}

retCode
    [Arguments]    ${ExcelFile}    ${Row}    ${Column}    ${input}
    #所属机构
    ${retCode}    set variable    ${input}
    addExcel    ${ExcelFile}    ${Row}    ${Column}    ${retCode}
    [Return]    ${retCode}    ${null}

retMsg
    [Arguments]    ${ExcelFile}    ${Row}    ${Column}    ${input}
    #所属机构
    ${retMsg}    set variable    ${input}
    addExcel    ${ExcelFile}    ${Row}    ${Column}    ${retMsg}
    [Return]    ${retMsg}    ${null}

sourceClient
    [Arguments]    ${ExcelFile}    ${Row}    ${Column}    ${input}
    #所属机构
    ${sourceClient}    set variable    ${input}
    addExcel    ${ExcelFile}    ${Row}    ${Column}    ${sourceClient}
    [Return]    ${sourceClient}    ${null}

customerName
    [Arguments]    ${ExcelFile}    ${Row}    ${Column}
    #姓名
    ${customerName}    customerNameRange
    addExcel    ${ExcelFile}    ${Row}    ${Column}    ${customerName}
    [Return]    ${customerName}    ${null}

degree
    [Arguments]    ${ExcelFile}    ${Row}    ${Column}
    #学历
    ${degree}    degreeRange
    addExcel    ${ExcelFile}    ${Row}    ${Column}    ${degree}
    [Return]    ${degree}    ${null}

CertType
    [Arguments]    ${ExcelFile}    ${Row}    ${Column}
    #证件类型
    ${CertType}    accCertTypeRange
    addExcel    ${ExcelFile}    ${Row}    ${Column}    ${CertType}
    [Return]    ${CertType}    ${null}

certId
    [Arguments]    ${ExcelFile}    ${Row}    ${Column}
    #身份证号
    ${certId}    certIdRangeB
    addExcel    ${ExcelFile}    ${Row}    ${Column}    ${certId}
    [Return]    ${certId}    ${null}

phone
    [Arguments]    ${ExcelFile}    ${Row}    ${Column}
    #手机号
    ${mobile}    phoneRange
    addExcel    ${ExcelFile}    ${Row}    ${Column}    ${mobile}
    [Return]    ${mobile}    ${null}

email
    [Arguments]    ${ExcelFile}    ${Row}    ${Column}
    #姓名
    ${email}    emailRange
    addExcel    ${ExcelFile}    ${Row}    ${Column}    ${email}
    [Return]    ${email}    ${null}

loanTerm
    [Arguments]    ${ExcelFile}    ${Row}    ${Column}
    #证件类型
    ${loanTerm}    termRange
    addExcel    ${ExcelFile}    ${Row}    ${Column}    ${loanTerm}
    [Return]    ${loanTerm}    ${null}

approveSuggestAmt
    [Arguments]    ${ExcelFile}    ${Row}    ${Column}    ${Start}    ${End}    ${Count}
    #姓名
    ${approveSuggestAmt}    floatRange    ${Start}    ${End}    ${Count}
    addExcel    ${ExcelFile}    ${Row}    ${Column}    ${approveSuggestAmt}
    [Return]    ${approveSuggestAmt}    ${null}

riskGrade
    [Arguments]    ${ExcelFile}    ${Row}    ${Column}
    #证件类型
    ${riskGrade}    riskGradeRange
    addExcel    ${ExcelFile}    ${Row}    ${Column}    ${riskGrade}
    [Return]    ${riskGrade}    ${null}

score
    [Arguments]    ${ExcelFile}    ${Row}    ${Column}    ${Start}    ${End}
    #姓名
    ${score}    intRange    ${Start}    ${End}
    addExcel    ${ExcelFile}    ${Row}    ${Column}    ${score}
    [Return]    ${score}    ${null}

productId
    [Arguments]    ${ExcelFile}    ${Row}    ${Column}    ${input}
    #学历
    ${productId}    set variable    ${input}
    addExcel    ${ExcelFile}    ${Row}    ${Column}    ${productId}
    [Return]    ${productId}    ${null}

oldAppId
    [Arguments]    ${ExcelFile}    ${Row}    ${Column}
    #姓名
    ${oldAppId}    oldAppIdRange
    addExcel    ${ExcelFile}    ${Row}    ${Column}    ${oldAppId}
    [Return]    ${oldAppId}    ${null}

borrowerType
    [Arguments]    ${ExcelFile}    ${Row}    ${Column}    ${input}
    #借款人类型
    ${borrowerType}    set variable    ${input}
    addExcel    ${ExcelFile}    ${Row}    ${Column}    ${borrowerType}
    [Return]    ${borrowerType}    ${null}

instCode
    [Arguments]    ${ExcelFile}    ${Row}    ${Column}    ${input}
    #所属机构
    ${instCode}    set variable    ${input}
    addExcel    ${ExcelFile}    ${Row}    ${Column}    ${instCode}
    [Return]    ${instCode}    ${null}

saleChannel
    [Arguments]    ${ExcelFile}    ${Row}    ${Column}    ${input}
    #所属机构
    ${saleChannel}    set variable    ${input}
    addExcel    ${ExcelFile}    ${Row}    ${Column}    ${saleChannel}
    [Return]    ${saleChannel}    ${null}

career
    [Arguments]    ${ExcelFile}    ${Row}    ${Column}
    #所属机构
    ${career}    careerRange
    addExcel    ${ExcelFile}    ${Row}    ${Column}    ${career}
    [Return]    ${career}    ${null}

liveAddress
    [Arguments]    ${ExcelFile}    ${Row}    ${Column}
    #所属机构
    ${liveAddress}    liveAddressRange
    addExcel    ${ExcelFile}    ${Row}    ${Column}    ${liveAddress}
    [Return]    ${liveAddress}    ${null}

marry
    [Arguments]    ${ExcelFile}    ${Row}    ${Column}
    #婚姻状况
    ${marry}    marryRange
    addExcel    ${ExcelFile}    ${Row}    ${Column}    ${marry}
    [Return]    ${marry}    ${null}

loanPurpose
    [Arguments]    ${ExcelFile}    ${Row}    ${Column}
    #所属机构
    ${loanPurpose}    loanPurposeRange
    addExcel    ${ExcelFile}    ${Row}    ${Column}    ${loanPurpose}
    [Return]    ${loanPurpose}    ${null}

key1
    [Arguments]    ${ExcelFile}    ${Row}    ${Column}
    #所属机构
    ${key1}    set variable    uumCustNo
    addExcel    ${ExcelFile}    ${Row}    ${Column}    ${key1}
    [Return]    ${key1}    ${null}

key2
    [Arguments]    ${ExcelFile}    ${Row}    ${Column}
    #所属机构
    ${key2}    set variable    uumUserId
    addExcel    ${ExcelFile}    ${Row}    ${Column}    ${key2}
    [Return]    ${key2}    ${null}

value1
    [Arguments]    ${ExcelFile}    ${Row}    ${Column}    ${input}
    #所属机构
    ${value1}    set variable    ${input}
    addExcel    ${ExcelFile}    ${Row}    ${Column}    ${value1}
    [Return]    ${value1}    ${null}

value2
    [Arguments]    ${ExcelFile}    ${Row}    ${Column}    ${input}
    #所属机构
    ${value2}    set variable    ${input}
    addExcel    ${ExcelFile}    ${Row}    ${Column}    ${value2}
    [Return]    ${value2}    ${null}

modelId
    [Arguments]    ${ExcelFile}    ${Row}    ${Column}    ${input}
    #所属机构
    ${modelId}    set variable    ${input}
    addExcel    ${ExcelFile}    ${Row}    ${Column}    ${modelId}
    [Return]    ${modelId}    ${null}

accBankName
    [Arguments]    ${ExcelFile}    ${Row}    ${Column}
    #所属机构
    ${accBankName}    accBankNameRange
    addExcel    ${ExcelFile}    ${Row}    ${Column}    ${accBankName}
    [Return]    ${accBankName}    ${null}

accBankBranch
    [Arguments]    ${ExcelFile}    ${Row}    ${Column}    ${accBankName}
    #所属机构
    ${accBankBranch}    accBankBranchRange    ${accBankName}
    addExcel    ${ExcelFile}    ${Row}    ${Column}    ${accBankBranch}
    [Return]    ${accBankBranch}    ${null}

accProvince
    [Arguments]    ${ExcelFile}    ${Row}    ${Column}
    #所属机构
    ${accProvince}    accProvinceRange
    addExcel    ${ExcelFile}    ${Row}    ${Column}    ${accProvince}
    [Return]    ${accProvince}    ${null}

accCity
    [Arguments]    ${ExcelFile}    ${Row}    ${Column}
    #所属机构
    ${accCity}    accCityRange
    addExcel    ${ExcelFile}    ${Row}    ${Column}    ${accCity}
    [Return]    ${accCity}    ${null}

accBankCard
    [Arguments]    ${ExcelFile}    ${Row}    ${Column}    ${accBankName}
    #所属机构
    ${accBankCard}    accBankCardRange    ${accBankName}
    addExcel    ${ExcelFile}    ${Row}    ${Column}    ${accBankCard}
    [Return]    ${accBankCard}    ${null}

accOwnName
    [Arguments]    ${ExcelFile}    ${Row}    ${Column}    ${input}
    #姓名
    ${accOwnName}    set variable    ${input}
    addExcel    ${ExcelFile}    ${Row}    ${Column}    ${accOwnName}
    [Return]    ${accOwnName}    ${null}

accOwnPhone
    [Arguments]    ${ExcelFile}    ${Row}    ${Column}
    #手机号
    ${accOwnPhone}    phoneRange
    addExcel    ${ExcelFile}    ${Row}    ${Column}    ${accOwnPhone}
    [Return]    ${accOwnPhone}    ${null}

accCertType
    [Arguments]    ${ExcelFile}    ${Row}    ${Column}
    #证件类型
    ${accCertType}    accCertTypeRange
    addExcel    ${ExcelFile}    ${Row}    ${Column}    ${accCertType}
    [Return]    ${accCertType}    ${null}

accOwnIdCard
    [Arguments]    ${ExcelFile}    ${Row}    ${Column}    ${input}
    #所属机构
    ${accOwnIdCard}    set variable    ${input}
    addExcel    ${ExcelFile}    ${Row}    ${Column}    ${accOwnIdCard}
    [Return]    ${accOwnIdCard}    ${null}

accountType
    [Arguments]    ${ExcelFile}    ${Row}    ${Column}    ${input}
    #所属机构
    ${accountType}    set variable    ${input}
    addExcel    ${ExcelFile}    ${Row}    ${Column}    ${accountType}
    [Return]    ${accountType}    ${null}

accType
    [Arguments]    ${ExcelFile}    ${Row}    ${Column}    ${input}
    #所属机构
    ${accType}    set variable    ${input}
    addExcel    ${ExcelFile}    ${Row}    ${Column}    ${accType}
    [Return]    ${accType}    ${null}

queryDB
    [Arguments]    ${sql}
    connDB    fbos    fbosdbcs    wcvIZL4bgJ    127.0.0.1    5568
    ${queryResult}    query    ${sql}
    [Return]    ${queryResult}

connDB
    [Arguments]    ${database}    ${user}    ${password}    ${host}    ${port}
    Connect To Database Using Custom Params    pymysql    database='${database}',user='${user}',password='${password}',host='${host}',port=${port}

trusteeType
    [Arguments]    ${ExcelFile}    ${Row}    ${Column}    ${input}
    #所属机构
    ${trusteeType}    set variable    ${input}
    addExcel    ${ExcelFile}    ${Row}    ${Column}    ${trusteeType}
    [Return]    ${trusteeType}    ${null}

accBankCardBindId
    [Arguments]    ${ExcelFile}    ${Row}    ${Column}    ${input}
    #所属机构
    ${accBankCardBindId}    set variable    ${input}
    addExcel    ${ExcelFile}    ${Row}    ${Column}    ${accBankCardBindId}
    [Return]    ${accBankCardBindId}    ${null}

orgnCreditCode
    [Arguments]    ${ExcelFile}    ${Row}    ${Column}    ${input}
    #所属机构
    ${orgnCreditCode}    set variable    ${input}
    addExcel    ${ExcelFile}    ${Row}    ${Column}    ${orgnCreditCode}
    [Return]    ${orgnCreditCode}    ${null}

accCorpRep
    [Arguments]    ${ExcelFile}    ${Row}    ${Column}    ${input}
    #所属机构
    ${accCorpRep}    set variable    ${input}
    addExcel    ${ExcelFile}    ${Row}    ${Column}    ${accCorpRep}
    [Return]    ${accCorpRep}    ${null}

toSort
    [Arguments]    ${ExcelFile}    ${Row}    ${Column}    ${input}
    #所属机构
    ${toSort}    set variable    ${input}
    addExcel    ${ExcelFile}    ${Row}    ${Column}    ${toSort}
    [Return]    ${toSort}    ${null}

transExeDate
    [Arguments]    ${ExcelFile}    ${Row}    ${Column}    ${input}
    #所属机构
    ${transExeDate}    set variable    ${input}
    addExcel    ${ExcelFile}    ${Row}    ${Column}    ${transExeDate}
    [Return]    ${transExeDate}    ${null}

transExeTime
    [Arguments]    ${ExcelFile}    ${Row}    ${Column}    ${input}
    #所属机构
    ${transExeTime}     set variable    ${input}
    addExcel    ${ExcelFile}    ${Row}    ${Column}    ${transExeTime}
    [Return]    ${transExeTime}    ${null}

transSerialNo
    [Arguments]    ${ExcelFile}    ${Row}    ${Column}    ${input}
    #所属机构
    ${transSerialNo}     set variable    ${input}
    addExcel    ${ExcelFile}    ${Row}    ${Column}    ${transSerialNo}
    [Return]    ${transSerialNo}    ${null}
