*** Settings ***
Library           RequestsLibrary
Library           Collections
Library           JFLibrary/JFLibrary.py
Library           ExcelLibrary

*** Variables ***

*** Test Cases ***
WebService
    @{paralist}    Create list
    ${url}    set variable    http://123.57.48.237:7082/webservice/loanService?wsdl
    ${filePath}    set variable    C:\\PyScripts\\WebServiceForWD\\param\\para.xls
    open excel    ${filePath}
    ${ColumnCount}    Get Column Count    Sheet1
    log    ${ColumnCount}
    : FOR    ${InputColumnCount}    IN RANGE    1    ${ColumnCount}
    \    @{paralist}    初始化webservice参数    ${filePath}    Sheet1    ${InputColumnCount}    @{paralist}
    \    ZhiZunBao    ${url}    @{paralist}

*** Keywords ***
初始化webservice参数
    [Arguments]    ${filePath}    ${sheet}    ${InputColumnCount}    @{paraList}
    open excel    ${filePath}
    ${RowCount}    Get Row Count    ${sheet}
    log    ${sheet}
    : FOR    ${i}    IN RANGE    ${RowCount}
    \    ${paraname}    Read Cell Data By coordinates    ${sheet}    ${InputColumnCount}    ${i}
    \    Insert Into List    ${paraList}    ${i}    ${paraname}
    [Return]    @{paraList}
