#encoding=utf-8

import suds
from suds import client
import sys

class JFLibrary:
	"""
	玖富公司robotframework测试库，主要进行接口测试
	"""
	ROBOT_LIBRARY_SCOPE = 'GLOBAL'
	ROBOT_LIBRARY_VERSION = 0.1
	
	def __init__(self):
		pass
		
	def ZhiZunBao(self,url,*args):
		client=suds.client.Client(url)
		print args[0]
		print args[1]
		#创建工厂对象 
		head = client.factory.create("ns0:transHead")
		body = client.factory.create("ns0:lonApp")
		modelBusinessData=client.factory.create("ns0:commonObject")
		entry=client.factory.create('ns0:entry')
		entry1=client.factory.create('ns0:entry')
		tlonAccount=client.factory.create("ns0:tLonAccount") #方款卡
		tlonAccount1=client.factory.create("ns0:tLonAccount") #还款卡

		#参数赋值
		
		head.fromSort = args[0]
		head.retCode=args[1]
		head.retMsg=args[2]
		head.sourceClient=args[3]

		body.customerName=args[4]
		body.degree=args[5]
		body.certId=args[6]
		body.certType=args[7]
		body.phone=args[8]
		body.email=args[9]
		body.loanTerm=args[10]
		body.approveSuggestAmt=args[11]
		body.riskGrade=args[12]
		body.score=args[13]
		body.productId=args[14]
		body.oldAppId=args[15]
		body.borrowerType=args[16]
		body.instCode=args[17]
		body.saleChannel=args[18]
		body.career=args[19]
		body.liveAddress=args[20]
		body.marry=args[21]
		body.loanPurpose=args[22]

		#...
		entry.key=args[23]
		entry.value=args[24]
		entry1.key=args[25]
		entry1.value=args[26]

		modelBusinessData.modelId=args[27]
		modelBusinessData.propertyMap.entry.append(entry)
		modelBusinessData.propertyMap.entry.append(entry1)

		body.modelBusinessData.append(modelBusinessData)


		tlonAccount.accBankName=args[28]
		tlonAccount.accBankBranch=args[29]
		tlonAccount.accProvince=args[30]
		tlonAccount.accCity=args[31]
		tlonAccount.accBankCard=args[32]
		tlonAccount.accOwnName=args[33]
		tlonAccount.accOwnPhone=args[34]
		tlonAccount.accCertType=args[35]
		tlonAccount.accOwnIdCard=args[36]
		tlonAccount.accountType=args[37]
		tlonAccount.accType=args[38]
		tlonAccount.trusteeType=args[39]
		tlonAccount.accBankCardBindId=args[40]


		tlonAccount1.accBankName=args[41]
		tlonAccount1.accBankBranch=args[42]
		tlonAccount1.accProvince=args[43]
		tlonAccount1.accCity=args[44]
		tlonAccount1.accBankCard=args[45]
		tlonAccount1.accOwnName=args[46]
		tlonAccount1.accOwnPhone=args[47]
		tlonAccount1.accCertType=args[48]
		tlonAccount1.accOwnIdCard=args[49]
		tlonAccount1.accountType=args[50]
		tlonAccount1.accType=args[51]
		tlonAccount1.trusteeType=args[52]
		tlonAccount1.accBankCardBindId=args[53]

		body.tLonAccountList.append(tlonAccount)
		body.tLonAccountList.append(tlonAccount1)


		#获取结果

		result=client.service.recordLoan(head,body)
		f=open("D:/result.txt",'w')
		f.write(str(result))
		f.close()
		print result