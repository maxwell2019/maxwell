#encoding=utf-8

import suds
from suds import client
import sys

class WBLibrary:
	"""
	玖富公司robotframework测试库，主要进行接口测试
	"""
	ROBOT_LIBRARY_SCOPE = 'GLOBAL'
	ROBOT_LIBRARY_VERSION = 0.1
	
	def __init__(self):
		pass
		
	def CreateFactoryObject(self,client,factoryObjectName):
		factoryObject = client.factory.create(factoryObjectName)
		return factoryObject
        
        def AddAttributeToFactoryObject(self,factoryObject,AttributeName,AttributeValue):
                factoryObject[AttributeName] = AttributeValue
                return factoryObject
        
        def AddObjectToObject(self,ObjectParent,ObjectChildName,ObjectChild):
                ObjectParent[ObjectChildName].append(ObjectChild)
                return ObjectParent
        
        def CreateClient(self,url):
                client=suds.client.Client(url)
                return client

        def GetResult(self,client,serviceName,*args):
		cmd='result = client.service.'+serviceName+'(*args)'
		exec(cmd)
		return result
