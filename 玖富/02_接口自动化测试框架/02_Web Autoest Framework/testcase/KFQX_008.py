# coding = utf-8
#import HTMLTestRunner
import time,os
import unittest
from selenium import webdriver
from Sharelibs import login,exitlogin
from sshtunnel import SSHTunnelForwarder
import mysql.connector
from Tools import TestobjectReadWriter
from Tools import Connect_db

class TestWebCase:
    def __init__(self,driver,testcase_file,test_sheet):
        self.driver = driver
        self.testcase_file = testcase_file
        self.test_sheet = test_sheet
        print('进入测试case')
        self.server, self.conn = self.login_mysql()
        print("数据库已连接")
        self.app_id = ''
        self.other_app_id = ''
        # 创建截图文件夹
        screenshotDir = os.path.abspath('./testResult/screenshopt/') + '\\KFQX_008'
        if not os.path.exists(screenshotDir):
            os.mkdir(screenshotDir)

    def screenShot(self,screenshotContent):
        curTime = time.strftime("%Y%m%d%H%M", time.localtime())
        screenshotPath = os.path.abspath('./testResult/screenshopt/') + '\\KFQX_008\\' + screenshotContent + curTime +  '.png'
        print(screenshotPath)
        self.driver.get_screenshot_as_file(screenshotPath)

    #读取用户信息
    def get_login_info(self):
        print('读取登录信息')
        login_info = TestobjectReadWriter.ReadWriter_ecxel(self.testcase_file, self.test_sheet)  # 读写
        row = 31##根据用例写入
        col = 'G'##根据用例写入
        Login_info = login_info.read_excel_logininfo(row, col)
        self.Login_username = Login_info[0]
        self.Login_password = Login_info[1]
        print(self.Login_username, self.Login_password)

    #登录数据库
    def login_mysql(self):
        connect = Connect_db.GetDB()
        server, conn = connect.conn_mysql()
        return server,conn

    # 准备数据
    def pre_data(self):
        self.customer_name = u"刘纯"
        self.cert_id = '310105198909070040'
        cursor = self.conn.cursor()
        print("创建此用户在外部机构的工单")
        # 插入此用户其他机构的工单
        cursor.execute(
            "INSERT INTO `pre_loan_db`.`t_lon_application` VALUES ('', NULL, NULL, 'JFB', NULL, 'F0200', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '110852', '3', NULL, NULL, NULL, NULL, '1000', NULL, '668810', '刘纯', NULL, NULL, NULL, 'B1301', '310105198909070040', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', NULL, NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '104', '10', '及时雨', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'F4101', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'BasicOrderProcess:2:17524', NULL, 'B1701', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'F3501', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '1', '1', '1', NULL, NULL, '1', '1', '0', NULL, '/bizpage/loanbefore/instancejsp/recorder/20161014_1552_record_104.jsp', '/bizpage/loanbefore/instancejsp/check/20161014_1552_check_104.jsp', '/bizpage/loanbefore/instancejsp/show/20161014_1552_show_104.jsp', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'zidonghuawbyi', '2016-12-15 14:46:51', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'N8701', 'N8701', '0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2016-12-15 14:46:51', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '100022611410', NULL, NULL, NULL, NULL, NULL, 'C', 'N8702', '1', NULL, NULL, NULL, 'B154001', NULL, NULL, 'c2103bd7-759c-477c-ab15-810d96213d03', NULL, NULL)")
        self.conn.commit()

        #查询insert的其他机构工单app_id
        cursor = self.conn.cursor()
        cursor.execute(
            "SELECT APP_ID from t_lon_application where cert_id='%s' and INST_CODE ='110852'  ORDER BY CREATE_TIME DESC LIMIT 1" % self.cert_id)
        result_set = cursor.fetchall()
        self.other_app_id = result_set[0][0]

        cursor.execute(
            "SELECT COUNT(*) from t_lon_application where cert_id='%s'" % self.cert_id)
        result_set = cursor.fetchall()
        self.pre_num = result_set[0][0]
        print("录单前此用户工单数为%s" % result_set[0][0])

    def test(self):
        self.get_login_info()
        login_bus = login.Login(self.Login_username, self.Login_password, self.driver)
        login_bus.login()
        self.pre_data()
        testResult,reason = self.loan_application()
        self.clear_data(testResult)
        exit_login = exitlogin.Exitlogin(self.driver)
        exit_login.exitlogin()
        return testResult,reason


    # 录单
    def loan_application(self):
        driver = self.driver
        try:
            print('进入录单页面')
            driver.switch_to.frame("fraLeft")
            print('step1:点击贷前管理')
            driver.find_element_by_id("span500").click()
            self.driver.implicitly_wait(20)
            time.sleep(5)
            print('step2:录单')
            # driver.find_element_by_id("span500095").waitForElementPresent(u'录单')
            # driver.find_element_by_id("span500095").verifyText(u'录单')
            driver.find_element_by_id("span500095").click()
            driver.implicitly_wait(20)
        except:
            self.screenShot("进入录单页面异常")
            reason = u'进入录单页面异常'
            print('Fail:进入录单页面异常')
            testResult = 'Fail'
            return testResult,reason

        #开始录单
        try:
            driver.switch_to.default_content()
            driver.switch_to.frame("fraMain")
            print("选择薪易贷")
            driver.find_element_by_id("productId")
            driver.implicitly_wait(20)
            driver.find_element_by_xpath("/html/body/form/table/tbody/tr[1]/td[2]/select/option[2]").click()
            print(driver.find_element_by_xpath("/html/body/form/table/tbody/tr[1]/td[2]/select/option[2]").text)
            print("输入用户名、身份证、借款人类型")
            driver.implicitly_wait(20)
            driver.find_element_by_id("customerName").send_keys(self.customer_name)
            driver.find_element_by_id("certId").send_keys(self.cert_id)
            driver.find_element_by_id("borrowerType")
            #选择个人借款
            driver.find_element_by_xpath("/html/body/form/table/tbody/tr[4]/td[2]/select/option[2]").click()
            driver.implicitly_wait(20)
        except:
            self.screenShot("输入录单信息异常")
            reason = u'输入录单信息异常'
            print('Fail:输入录单信息异常')
            testResult = 'Fail'
            return testResult, reason

        try:
            driver.find_element_by_xpath("/html/body/form/div[2]/ul/li/input").click()
            driver.implicitly_wait(20)
            time.sleep(5)
            #下一步
            print("下一步")
            # driver.find_element_by_id("improveCustem").waitForElementPresent("下一步")
            driver.find_element_by_id("improveCustem").click()
            driver.implicitly_wait(20)
            driver.switch_to.default_content()
            driver.switch_to.frame("fraMain")
            #暂存
            driver.find_element_by_xpath("/html/body/div[2]/ul/li[3]/input").click()
            #确定
            driver.find_element_by_xpath("/html/body/div[5]/div[2]/div[3]/a").click()
            driver.implicitly_wait(20)
        except:
            self.screenShot("录单异常")
            reason = u'录单异常'
            print('Fail:录单异常')
            testResult = 'Fail'
            return testResult,reason

        #查询数据库是否录单成功
        cursor = self.conn.cursor()
        cursor.execute(
            "SELECT COUNT(*) from t_lon_application where cert_id='%s'" %self.cert_id)
        result_set = cursor.fetchall()
        new_num = result_set[0][0]
        print(new_num)
        print("录单后工单总数为：%s" %new_num)
        if(new_num == (self.pre_num+1)):
            print("录单成功")
        else:
            reason = u'录单失败'
            print('Fail:录单失败')
            testResult = 'Fail'
            return testResult, reason

        cursor = self.conn.cursor()
        cursor.execute(
            "SELECT APP_ID,INST_CODE from t_lon_application where cert_id='%s' ORDER BY CREATE_TIME DESC LIMIT 1" % self.cert_id)
        result_set = cursor.fetchall()
        self.app_id = result_set[0][0]
        inst_code = result_set[0][1]
        print("工单号为：%s,机构代码为：%s" % (self.app_id, inst_code))

        if inst_code != '11':
            reason = u'工单机构代码验证失败'
            print('Fail:工单机构代码验证失败')
            testResult = 'Fail'
            return testResult, reason

        testResult,reason = self.query()
        return testResult,reason

    def query(self):
        # 工单查询（客服）验证
        driver = self.driver
        try:
            print("进入工单查询（客服）界面")
            driver.switch_to.default_content()
            driver.switch_to.frame("fraLeft")
            driver.find_element_by_id("span500031").click()
            print("进入录单查询界面")
            driver.implicitly_wait(20)
            # time.sleep(5)
        except:
            self.screenShot("进入工单查询（客服）异常")
            reason = u'进入工单查询（客服）异常'
            print('Fail:进入工单查询（客服）异常')
            testResult = 'Fail'
            return testResult, reason

        # 根据app_id查询
        try:
            print("根据app_id查询")
            driver.switch_to.default_content()
            driver.switch_to.frame("fraMain")
            driver.find_element_by_id("appId").send_keys(self.app_id)
            driver.implicitly_wait(20)
            driver.find_element_by_id("btnListQuery").click()
            driver.implicitly_wait(20)
            # time.sleep(10)
        except:
            self.screenShot("工单查询（客服）查询异常")
            reason = u'工单查询（客服）查询异常'
            print('Fail:进入工单查询（客服）查询异常')
            testResult = 'Fail'
            return testResult, reason

        print("获取产品名称")
        try:
            print(driver.find_element_by_xpath("/html/body/form/table/tbody").text)
            id = driver.find_element_by_xpath("/html/body/form/table/tbody/tr/td[6]").text
            print(id)
            if (id != u'薪易贷'):
                reason = u'工单产品名称错误'
                print('Fail:工单产品名称错误')
                testResult = 'Fail'
                return testResult,reason
        except:
            reason = u'未查询到此工单'
            print('Fail:未查询到此工单')
            testResult = 'Fail'
            return testResult, reason

        # 验证其他机构录单不能查询到
        #查询其他机构的app_id
        try:
            print("根据app_id查询其他机构工单")
            driver.switch_to.default_content()
            driver.switch_to.frame("fraMain")
            driver.find_element_by_id("appId").clear()
            driver.find_element_by_id("appId").send_keys(self.other_app_id)
            driver.implicitly_wait(20)
            driver.find_element_by_id("btnListQuery").click()
            driver.implicitly_wait(20)
            # time.sleep(10)
        except:
            self.screenShot("其他机构工单查询异常")
            reason = u'其他机构工单查询异常'
            print('Fail:其他机构工单查询异常')
            testResult = 'Fail'
            return testResult, reason
        #没有查询到pass，其他fail
        try:
            id = driver.find_element_by_xpath("/html/body/form/table/tbody").text
            if id == "":
                print('Pass')
                testResult = 'Pass'
                reason = ''
            else:
                print('Fail')
                testResult = 'Fail'
                reason = u"权限验证失败：可以查询到其他机构的工单"
        except:
            self.screenShot("其他机构工单验证异常")
            reason = u'其他机构工单验证异常'
            print('Fail:其他机构工单验证异常')
            testResult = 'Fail'

        return testResult,reason


    def clear_data(self, testResult):
        cursor = self.conn.cursor()
        if self.other_app_id !='':
            print("DELETE from t_lon_application where app_id= '%s'" % self.other_app_id)
            cursor.execute(
                "DELETE from t_lon_application where app_id= '%s'" % self.other_app_id)
            self.conn.commit()

        if self.app_id !='' and testResult == 'Pass':
            print("DELETE from t_lon_application where app_id= '%s'" % self.app_id)
            cursor.execute(
                "DELETE from t_lon_application where app_id= '%s'" % self.app_id)
            self.conn.commit()
        self.server.stop()


