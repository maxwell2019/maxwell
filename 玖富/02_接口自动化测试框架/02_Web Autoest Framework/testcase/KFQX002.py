# -*- coding:utf-8 -*-
#
__author__ = 'fubo'
import sys, os
from selenium import webdriver
import json
import time
from selenium.webdriver.common.keys import Keys
from Sharelibs import login
import time
import logging
import unittest

class TestWebCase:

    def __init__(self,driver):
        self.driver = driver
        print('进入测试case')

    def test(self):
#        driver = driver
#    #pre_login = ()#登录
#        login.login(self)#登录
            #username = self.predata_dianxiao()##数据准备
        try:
                #print('进入商机管理页面')
            self.driver.switch_to_frame("fraLeft")
                #点贷前管理
            self.driver.find_element_by_id("span500").click()
            self.driver.implicitly_wait(20)
                #点商机预处理
            self.driver.find_element_by_id("span500111").waitForElementPresent('商机预处理')

            self.driver.find_element_by_id("span500111").verifyText('商机预处理')
            self.driver.find_element_by_id("span500111").click()

            self.driver.implicitly_wait(30)
        except:
            self.driver.get_screenshot_as_file("E:\9F\贷我飞\理财后台web自动化\error_进入商机管理页面失败.png")
            print('进入商机管理页面失败')
            username = ''
            return(username)

    ##获取查询信息
        try:
    #print('获取要查询的信息...')
            self.driver.switch_to_default_content()##回到默认
            self.driver.switch_to_frame("fraMain")
                #driver.switch_to_frame("loanBefore")
                #print('获取用户名...')
            username = self.driver.find_element_by_xpath("/html/body/form/table/tbody/tr[1]/td[5]").text
            self.driver.implicitly_wait(5)
                #print('获取手机号码...')
            PhoneNo = self.driver.find_element_by_xpath("/html/body/form/table/tbody/tr[1]/td[6]").text
            self.driver.implicitly_wait(5)
                #print('获取申请开始时间...')
            begintime = self.driver.find_element_by_xpath("/html/body/form/table/tbody/tr[1]/td[4]").text
            self.driver.implicitly_wait(5)
            #asser

        except:
            self.driver.get_screenshot_as_file("E:\9F\贷我飞\理财后台web自动化\error_获取要查询的信息失败.png")
            print('获取要查询的信息失败')
            username = ''
            return(username)

            #print('获取到的查询信息-用户名：%s' %username)
            #print('获取到的查询信息-手机号码：%s' %PhoneNo)
            #print('获取到的查询信息-申请开始时间：%s' %begintime)

    ##输入查询条件
        try:
                #print('输入查询信息-姓名-手机号码-申请开始时间......')
            self.driver.switch_to_default_content()##回到默认
            self.driver.switch_to_frame("fraMain")
                #driver.switch_to_frame("loanBefore")

                #输入查询姓名
            self.driver.find_element_by_xpath("/html/body/form/div[1]/ul/li[1]/label/input").send_keys(u"%s" %username)

                #输入手机号码
            self.driver.find_element_by_xpath("/html/body/form/div[1]/ul/li[5]/label/input").send_keys(u"%s" %PhoneNo)

                #输入申请开始时间
            self.driver.find_element_by_xpath("/html/body/form/div[1]/ul/li[6]/label/input").send_keys(u"%s" %begintime)
            self.driver.implicitly_wait(5)

        except:
            self.driver.get_screenshot_as_file("E:\9F\贷我飞\理财后台web自动化\error_输入查询信息失败.png")
            print('输入查询信息失败')
            username = ''
            return(username)

    ##查询
        try:
                #print('查询......')
            self.driver.switch_to_default_content()##回到默认
            self.driver.switch_to_frame("fraMain")
                #driver.switch_to_frame("loanBefore")
            username = driver.find_element_by_xpath("/html/body/form/div[1]/ul/li[10]/button[2]").click()
            self.driver.implicitly_wait(15)

        except:
            self.driver.get_screenshot_as_file("E:\9F\贷我飞\理财后台web自动化\error_查询失败.png")
            print('查询失败')
            username = ''
            return(username)

        return(username,PhoneNo,begintime)

    def result_confirm(self):
#        driver = setUp(self)
        testinfo = case.test(self)#执行
        username = testinfo[0]
        phoneNo = testinfo[1]
        begintime = testinfo[2]
        time.sleep(3)

        if username == '':
            print("测试中止")
        else:
            time.sleep(1)
                #print('验证查询结果是否正确...')
            try:
                time.sleep(2)
                    #print('获取查询结果信息...')
                self.driver.switch_to_default_content()##回到默认
                self.driver.switch_to_frame("fraMain")
                    #driver.switch_to_frame("loanBefore")
                username_result = self.driver.find_element_by_xpath("/html/body/form/table/tbody/tr/td[5]").text#获取查询结果的姓名
                phoneNo_result = self.driver.find_element_by_xpath("/html/body/form/table/tbody/tr/td[6]").text#获取查询结果的手机号码
                begintime_result = self.driver.find_element_by_xpath("/html/body/form/table/tbody/tr/td[4]").text#获取查询结果的申请时间
                self.driver.implicitly_wait(5)

                    ##保存查询结果页面图片
                    #print('获取查询结果信息是-姓名：%s' %username_result)
                    #print('获取查询结果信息是-手机号码：%s' %phoneNo_result)
                    #print('获取查询结果信息是-申请时间：%s' %begintime_result)

            except:
                self.driver.get_screenshot_as_file("E:\9F\贷我飞\理财后台web自动化\error_获取查询结果信息.png")
                print('获取查询结果信息失败')

                if  phoneNo_result == phoneNo:
                    print('Pass',end='')
                    testresult = 'Pass'

                else:
                    print('Fail',end='')
                    testresult = 'Fail'
            return(testresult)


#    def tearDown(self):
#        driver = self.browser
#        driver.quit()