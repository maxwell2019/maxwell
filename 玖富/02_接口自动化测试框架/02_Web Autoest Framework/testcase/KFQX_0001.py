#coding:utf-8

import time
from selenium.webdriver.common.keys import Keys
from Tools import TestobjectReadWriter
from Sharelibs import  login,exitlogin
from Tools import Connect_db

class TestWebCase:
    def __init__(self,driver,testcase_file, test_sheet):
        self.Driver = driver
        self.testcase_file = testcase_file
        self.test_sheet = test_sheet
        print('进入测试case')
        self.server, self.conn = Connect_db.GetDB().conn_mysql()
        self.verificationErrors = []
        self.accept_next_alert = True


        # 读取用户登录信息
    def get_login_info(self):
        print('读取登录信息')
        login_info = TestobjectReadWriter.ReadWriter_ecxel(self.testcase_file, self.test_sheet)  # 读写
        row = 3  ##根据用例写入
        col = 'G'  ##根据用例写入
        Login_info = login_info.read_excel_logininfo(row, col)
        self.Login_username = Login_info[0]
        self.Login_password = Login_info[1]
        print(self.Login_username, self.Login_password)

        # 准备数据
    def pre_data(self):
        self.customer_Name = "王丽"
        self.cert_Id = '62010519801228002X'
        # 其他客服zidonghuaer创建的APP_ID
        self.LuDanGuanLi_Other_APP_ID = "22608154"
        # 其他客服zidonghuaer创建的APP_ID
        self.GongDanChaXunKF_Other_APP_ID = "22608154"
        # 其他客服zidonghuaer创建的APP_ID
        self.FuYiGongDanChaXun_Other_APP_ID = "20207410"

    def test(self):
        self.pre_data()
        self.get_login_info()
        login_bus = login.Login(self.Login_username, self.Login_password, self.Driver)
        login_bus.login()
        #self.Driver = self.driver
        try:
            self.LuDan()
        except:
            pass
        try:
            self.LuDanGuanLi()
        except:
            pass
        try:
            self.GongDanChaXun()
        except:
            pass
        try:
            self.FuYiGongDanChaXun()
        except:
            pass

        testresult = self.result_confirm()
        exit_login = exitlogin.Exitlogin(self.Driver)
        exit_login.exitlogin()

        return  testresult

    def result_confirm(self):
        print("==========实际结果与预期结果对比=============")

        #贷前管理——录单
        try:
            if self.MySQL_APP_ID == "11010101":
                testresult_LuDan = 'pass'
                print("贷前管理—录单:pass", end='')

            else:
                print("贷前管理—录单:fail", end='')
                testresult_LuDan = 'fail'
        except:
            print("贷前管理—录单:程序异常")
            print('贷前管理—录单:fail', end='')
            testresult_LuDan = 'fail'

        #贷前管理—录单管理
        try:
            if self.LuDanGuanLi_APP_ID == "":
                print("贷前管理-录单管理:pass")
                testresult_LuDanGuanLi = 'pass'

            else:
                print("贷前管理-录单管理:fail")
                testresult_LuDanGuanLi = 'fail'
                testresult_LuDanGuanLi_reason = '可以获取其他客服的工单编号'
                print(self.LuDanGuanLi_APP_ID)
                self.Driver.get_screenshot_as_file("D:\BusError\error_录单管理-可以获取其他客服的工单编号.png")
        except:
            print("贷前管理-录单管理:程序异常")
            print('贷前管理-录单管理:fail', end='')
            testresult_LuDanGuanLi = 'fail'

        #贷前管理-工单查询（客服）
        try:
            if self.GongDanChaXunKF_APP_ID == "":
                testresult_GongDanChaXunKF = 'pass'
                print("贷前管理-工单查询(客服):pass")
            else:
                testresult_GongDanChaXunKF = 'pass'
                testresult_GongDanChaXunKF_reason = '可以获取其他客服的工单编号'
                print("贷前管理-工单查询(客服):fail")
                print(self.GongDanChaXunKF_APP_ID)
                self.Driver.get_screenshot_as_file("D:\BusError\error_工单查询(客服)_可以获取其他客服的工单编号.png")
        except:
            print("贷前管理-工单查询(客服):程序异常")
            print('贷前管理-工单查询(客服):fail', end='')
            testresult_GongDanChaXunKF = 'fail'

        #贷前管理-复议工单查询
        try:
            if self.FYGDCXGL_APP_ID == "":
                print("贷前管理-复议工单查询管理:pass")
                testresult_FYGDCXGL = 'pass'
            else:
                testresult_FYGDCXGL = 'fail'
                testresult_FYGDCXGL_reason = '可以获取其他客服的工单编号'
                print("贷前管理-复议工单查询管理:fail")
                print(self.FYGDCXGL_APP_ID)
                self.Driver.get_screenshot_as_file("D:\BusError\error_复议工单查询管理_可以获取其他客服的工单编号.png")
        except:
            print("贷前管理-复议工单查询管理:程序异常")
            print('贷前管理-复议工单查询管理:fail', end='')
            testresult_FYGDCXGL = 'fail'

        return testresult_LuDan
        #,testresult_LuDanGuanLi,testresult_GongDanChaXunKF,testresult_FYGDCXGL ##fubo 12/20


    def LuDan(self):
        print("=========================================")
        print('贷前管理-录单')
        #开始录单
        try:
            self.Driver.switch_to.default_content()
            self.Driver.switch_to.frame("fraLeft")
            print('step1:点击贷前管理')
            self.Driver.find_element_by_id("span500").click()
        except Exception as e:
            print(e)
            print('capture exception %s' % u"贷前管理按钮未找到")
            self.Driver.get_screenshot_as_file("D:\BusError\error_贷前管理按钮页面.png")
        self.Driver.implicitly_wait(20)
        try:
            self.Driver.switch_to.default_content()
            self.Driver.switch_to.frame("fraLeft")
            print('step2:点击录单')
            self.Driver.find_element_by_id("span500095").click()
        except Exception as e:
            print(e)
            print('capture exception %s' % u"贷前管理-录单按钮未找到")
            self.Driver.get_screenshot_as_file("D:\BusError\error_进入录单页面.png")
        self.Driver.implicitly_wait(20)
        try:
            self.Driver.switch_to.default_content()
            self.Driver.switch_to.frame("fraMain")
            print('    选择产品名称.....')
            self.Driver.find_element_by_id("productId")
            self.Driver.implicitly_wait(20)
            self.Driver.find_element_by_xpath("/html/body/form/table/tbody/tr[1]/td[2]/select/option[2]").click()
        except Exception as e:
            print(e)
            print('capture exception %s' % u"产品名称下拉选项未找到")
            self.Driver.get_screenshot_as_file("D:\BusError\error_产品名称.png")
        self.Driver.implicitly_wait(20)
        try:
            self.Driver.switch_to.default_content()
            self.Driver.switch_to.frame("fraMain")
            print('    录入用户身份信息.....')
            self.Driver.find_element_by_id("customerName").send_keys(self.customer_Name)
            self.Driver.find_element_by_id("certId").send_keys(self.cert_Id)
        except Exception as e:
            print(e)
            print('capture exception %s' % u"用户身份信息文本框未找到")
            self.Driver.get_screenshot_as_file("D:\BusError\error_录入身份信息.png")
        self.Driver.implicitly_wait(20)
        try:
            self.Driver.switch_to.default_content()
            self.Driver.switch_to.frame("fraMain")
            print('    选择借款人类型.....')
            borrowerTypeTextModel=self.Driver.find_element_by_xpath("/html/body/form/table/tbody/tr[4]/td[2]/select")
            borrowerTypeTextModel.find_element_by_xpath("//option[@value='B154001']").click()
        except Exception as e:
            print(e)
            print('capture exception %s' % u"借款人文本框未找到")
            self.Driver.get_screenshot_as_file("D:\BusError\error_借款人类型.png")
        self.Driver.implicitly_wait(20)
        try:
            self.Driver.switch_to.default_content()
            self.Driver.switch_to.frame("fraMain")
            print('step3:点击查询按钮.....')
            self.Driver.find_element_by_xpath("/html/body/form/div[2]/ul/li/input").click()
        except Exception as e:
            print(e)
            print('capture exception %s' % u"查询按钮未找到")
            self.Driver.get_screenshot_as_file("D:\BusError\error_查询页面.png")
            print('点击查询按钮失败')
        time.sleep(5)
        self.Driver.implicitly_wait(20)
        try:
            self.Driver.switch_to.default_content()
            self.Driver.switch_to.frame("fraMain")
            for i in range(1,20):
                self.Driver.find_element_by_xpath("/html/body").send_keys(Keys.DOWN)
            self.Driver.implicitly_wait(5)
            print('step4:点击下一步按钮.....')
            self.Driver.find_element_by_xpath("/html/body/form/div[7]/div/ul/li[1]/input").click()
        except Exception as e:
            print(e)
            print('capture exception %s' % u"下一步按钮未找到")
            time.sleep(5)
            self.Driver.get_screenshot_as_file("D:\BusError\error_下一步.png")
            print('点击下一步按钮失败')
            #点击主页按钮，返回到主页
        try:
            self.Driver.switch_to.default_content()
            self.Driver.switch_to.frame("fraLeft")
            print('点击主页按钮，返回主页')
            self.Driver.find_element_by_xpath(".//*[@id='main']/div[2]/a").click()
        except Exception as e:
             print(e)
             print('capture exception %s' % u"主页按钮未找到")
             self.Driver.get_screenshot_as_file("D:\BusError\error_反回到主页.png")
        try:
            try:
                # print('进入贷前管理页面')
                self.Driver.switch_to.default_content()
                self.Driver.switch_to.frame("fraLeft")
                # print('step1:点击贷前管理')
                self.Driver.find_element_by_id("span500").click()
            except Exception as e:
                print(e)
                print('capture exception %s' % u"贷前管理按钮未找到")
                self.Driver.get_screenshot_as_file("D:\BusError\error_贷前管理按钮页面.png")
        except:
            pass
        self.Driver.implicitly_wait(20)
        try:
            self.Driver.switch_to.default_content()
            self.Driver.switch_to.frame("fraLeft")
            # print('step2:点击录单')
            self.Driver.find_element_by_id("span500095").click()
        except Exception as e:
            print(e)
            print('capture exception %s' % u"贷前管理-录单按钮未找到")
            self.Driver.get_screenshot_as_file("D:\BusError\error_进入录单页面.png")
        self.Driver.implicitly_wait(20)
        try:
            self.Driver.switch_to.default_content()
            self.Driver.switch_to.frame("fraMain")
            # print('    选择产品名称.....')
            self.Driver.find_element_by_id("productId")
            self.Driver.implicitly_wait(20)
            self.Driver.find_element_by_xpath("/html/body/form/table/tbody/tr[1]/td[2]/select/option[2]").click()
        except Exception as e:
            print(e)
            print('capture exception %s' % u"产品名称下拉选项未找到")
            self.Driver.get_screenshot_as_file("D:\BusError\error_产品名称.png")
        self.Driver.implicitly_wait(20)
        try:
            self.Driver.switch_to.default_content()
            self.Driver.switch_to.frame("fraMain")
            # print('    录入用户身份信息.....')
            self.Driver.find_element_by_id("customerName").send_keys(self.customer_Name)
            self.Driver.find_element_by_id("certId").send_keys(self.cert_Id)
        except Exception as e:
            print(e)
            print('capture exception %s' % u"用户身份信息文本框未找到")
            self.Driver.get_screenshot_as_file("D:\BusError\error_录入身份信息.png")
        self.Driver.implicitly_wait(20)
        try:
            self.Driver.switch_to.default_content()
            self.Driver.switch_to.frame("fraMain")
            # print('    选择借款人类型.....')
            borrowerTypeTextModel=self.Driver.find_element_by_xpath("/html/body/form/table/tbody/tr[4]/td[2]/select")
            borrowerTypeTextModel.find_element_by_xpath("//option[@value='B154001']").click()
        except Exception as e:
            print(e)
            print('capture exception %s' % u"借款人文本框未找到")
            self.Driver.get_screenshot_as_file("D:\BusError\error_借款人类型.png")
        self.Driver.implicitly_wait(20)
        try:
            self.Driver.switch_to.default_content()
            self.Driver.switch_to.frame("fraMain")
            # print('step3:点击查询按钮.....')
            self.Driver.find_element_by_xpath("/html/body/form/div[2]/ul/li/input").click()
        except Exception as e:
            print(e)
            print('capture exception %s' % u"查询按钮未找到")
            self.Driver.get_screenshot_as_file("D:\BusError\error_查询页面.png")
            print('点击查询按钮失败')
        self.Driver.implicitly_wait(20)
        try:
            self.Driver.switch_to.default_content()
            self.Driver.switch_to.frame("fraMain")
            print('获取工单编号信息.....')
            # self.APP_ID=self.Driver.find_element_by_xpath(".//*[@id='tblResultOfSglTbl']/tbody[2]/tr[1]/td[1]").text
            a=self.Driver.find_element_by_css_selector("html body form#recorderForm div#connectRecorde table#tblResultOfSglTbl.simpleListTable.pagesortable.msgrids tbody tr:last-child")
            self.APP_ID=a.find_element_by_css_selector("#tblResultOfSglTbl>tbody>tr>td:first-child").text
            print(self.APP_ID)
        except Exception as e:
            print(e)
            print('capture exception %s' % u"工单编号为获取到")
            self.Driver.get_screenshot_as_file("D:\BusError\error_工单编号.png")
            print('获取工单编号失败')

        try:

            print("连接数据库做查询")
            cursor = self.conn.cursor()
            try:
                cursor.execute("SELECT INST_CODE FROM t_lon_application WHERE APP_ID='%s'" %self.APP_ID)
                result_set = cursor.fetchall()
                if result_set:
                    for row in result_set:
                        self.MySQL_APP_ID=row[0]
                        print (self.MySQL_APP_ID)
                print('关闭数据库服务')
                cursor.close()
                self.conn.close()
                self.server.stop()
            except Exception as e:
                print(e)
                print('capture exception %s' % u"查询信息不存在")
                print('查询信息不存在')
        except Exception as e:
            print(e)
            print('capture exception %s' % u"数据库连接失败，获取客服权限信息失败")
            print('数据库连接失败，获取客服权限信息失败')

    def LuDanGuanLi(self):
        #点击主页按钮，返回到主页
        print("=========================================")
        print('贷前管理-录单管理')
        try:
            self.Driver.switch_to.default_content()
            self.Driver.switch_to.frame("fraLeft")
            print('点击主页按钮，返回主页')
            self.Driver.find_element_by_xpath(".//*[@id='main']/div[2]/a").click()
        except Exception as e:
             print(e)
             print('capture exception %s' % u"主页按钮未找到")
             self.Driver.get_screenshot_as_file("D:\BusError\error_反回到主页.png")
        try:
            try:
                print('进入录单管理页面')
                self.Driver.switch_to.default_content()
                self.Driver.switch_to.frame("fraLeft")
                print('step1:点击贷前管理')
                self.Driver.find_element_by_id("span500").click()
            except Exception as e:
                print(e)
                print('capture exception %s' % u"贷前管理按钮未找到")
                self.Driver.get_screenshot_as_file("D:\BusError\error_贷前管理按钮页面.png")
        except:
            pass
        self.Driver.implicitly_wait(20)
        # 进入录单管理页面
        try:
            self.Driver.switch_to.default_content()
            self.Driver.switch_to.frame("fraLeft")
            print('step2:点击录单管理')
            self.Driver.find_element_by_id("span500005").click()
        except Exception as e:
            print(e)
            print('capture exception %s' % u"贷前管理-录单管理按钮未找到")
            self.Driver.get_screenshot_as_file("D:\BusError\error_进入录单管理页面.png")
        self.Driver.implicitly_wait(20)
        #输入zidonghuaer客服建立的工单编号
        try:
            self.Driver.switch_to.default_content()
            self.Driver.switch_to.frame("fraMain")
            print('    输入zidonghuaer客服建立的工单编号....')
            self.Driver.find_element_by_xpath("/html/body/form/div[2]/ul[1]/li[1]/label/input").send_keys(self.LuDanGuanLi_Other_APP_ID)
        except Exception as e:
            print(e)
            print('capture exception %s' % u"输入工单编号文本框未找到")
            self.Driver.get_screenshot_as_file("D:\BusError\error_工单编号文本框页面.png")
        self.Driver.implicitly_wait(20)
        #点击录单管理查询按钮
        try:
            self.Driver.switch_to.default_content()
            self.Driver.switch_to.frame("fraMain")
            print("step3:点击录单管理-查询按钮")
            self.Driver.find_element_by_xpath("/html/body/form/div[2]/ul[2]/li[3]/button[2]").click()
        except Exception as e:
            print(e)
            print('capture exception %s' % u"录单管理查询按钮未找到")
            self.Driver.get_screenshot_as_file("D:\BusError\error_录单管理查询按钮页面.png")
        self.Driver.implicitly_wait(20)
         #录单管理-获取工单编号
        try:
            self.Driver.switch_to.default_content()
            self.Driver.switch_to.frame("fraMain")
            print('    获取查询结果...')
            self.LuDanGuanLi_APP_ID=self.Driver.find_element_by_xpath("/html/body/form/table/tbody").text
            return self.LuDanGuanLi_APP_ID
        except Exception as e:
            print(e)
            print('capture exception %s' % u"录单管理获取工单编号失败")
            self.Driver.get_screenshot_as_file("D:\BusError\error_获取工单编号页面.png")
        self.Driver.implicitly_wait(20)


    def GongDanChaXun(self):
         #点击主页按钮，返回到主页
        print("=========================================")
        print('贷前管理-工单查询(客服)')
        try:
            self.Driver.switch_to.default_content()
            self.Driver.switch_to.frame("fraLeft")
            print('点击主页按钮，返回主页')
            self.Driver.find_element_by_xpath(".//*[@id='main']/div[2]/a").click()
        except Exception as e:
             print(e)
             print('capture exception %s' % u"主页按钮未找到")
             self.Driver.get_screenshot_as_file("D:\BusError\error_返回到主页.png")
        self.Driver.implicitly_wait(20)
        try:
            try:
                print('进入工单查询(客服)页面')
                self.Driver.switch_to.default_content()
                self.Driver.switch_to.frame("fraLeft")
                print('step1:点击贷前管理')
                self.Driver.find_element_by_id("span500").click()
            except Exception as e:
                print(e)
                print('capture exception %s' % u"贷前管理按钮未找到")
                self.Driver.get_screenshot_as_file("D:\BusError\error_贷前管理按钮页面.png")
        except:
            pass
        self.Driver.implicitly_wait(20)
        # 进入工单查询(客服)页面
        try:
            self.Driver.switch_to.default_content()
            self.Driver.switch_to.frame("fraLeft")
            print('step2:点击工单查询(客服)')
            self.Driver.find_element_by_id("span500031").click()
        except Exception as e:
            print(e)
            print('capture exception %s' % u"贷前管理-工单查询(客服)按钮未找到")
            self.Driver.get_screenshot_as_file("D:\BusError\error_进入工单查询(客服)页面.png")
        self.Driver.implicitly_wait(20)
        #输入zidonghuaer客服建立的工单编号
        try:
            self.Driver.switch_to.default_content()
            self.Driver.switch_to.frame("fraMain")
            print("   输入zidonghuaer客服建立的工单编号")
            self.Driver.find_element_by_xpath("/html/body/form/div[3]/ul[1]/li[1]/label/input").send_keys(self.GongDanChaXunKF_Other_APP_ID)
        except Exception as e:
            print(e)
            print('capture exception %s' % u"贷前管理-工单查询(客服)-录入工单编号文本框未找到")
            self.Driver.get_screenshot_as_file("D:\BusError\error_工单查询(客服)_工单编号文本框页面.png")
        self.Driver.implicitly_wait(20)
        #工单查询(客服)-查询按钮
        try:
            self.Driver.switch_to.default_content()
            self.Driver.switch_to.frame("fraMain")
            print("step3:点击工单查询(客服)-查询按钮")
            self.Driver.find_element_by_xpath("/html/body/form/div[3]/ul[3]/li[5]/button[2]").click()
        except Exception as e:
            print(e)
            print('capture exception %s' % u"贷前管理-工单查询(客服)-查询按钮未找到")
            self.Driver.get_screenshot_as_file("D:\BusError\error_工单查询(客服)_查询按钮页面.png")
        self.Driver.implicitly_wait(20)
        #工单查询(客服)-获取工单编号
        try:
            self.Driver.switch_to.default_content()
            self.Driver.switch_to.frame("fraMain")
            print("    获取查询结果....")
            self.GongDanChaXunKF_APP_ID=self.Driver.find_element_by_xpath("/html/body/form/table/tbody").text
            return self.GongDanChaXunKF_APP_ID
        except Exception as e:
            print(e)
            print('capture exception %s' % u"贷前管理-工单查询(客服)-查询结果未获取到")
            self.Driver.get_screenshot_as_file("D:\BusError\error_工单查询(客服)_获取工单编号页面.png")
        self.Driver.implicitly_wait(20)

    def FuYiGongDanChaXun(self):

        #点击主页按钮，返回到主页
        print("=========================================")
        print('贷前管理-复议工单查询管理')
        try:
            self.Driver.switch_to.default_content()
            self.Driver.switch_to.frame("fraLeft")
            print('点击主页按钮，返回主页')
            self.Driver.find_element_by_xpath(".//*[@id='main']/div[2]/a").click()
        except Exception as e:
             print(e)
             print('capture exception %s' % u"主页按钮未找到")
             self.Driver.get_screenshot_as_file("D:\BusError\error_返回到主页.png")
        self.Driver.implicitly_wait(20)
        try:
            try:
                print('进入复议工单查询管理页面')
                self.Driver.switch_to.default_content()
                self.Driver.switch_to.frame("fraLeft")
                print('step1:点击贷前管理')
                self.Driver.find_element_by_id("span500").click()
            except Exception as e:
                print(e)
                print('capture exception %s' % u"贷前管理按钮未找到")
                self.Driver.get_screenshot_as_file("D:\BusError\error_贷前管理按钮页面.png")
        except:
            pass
        self.Driver.implicitly_wait(20)
        # 进入复议工单查询管理页面
        try:
            self.Driver.switch_to.default_content()
            self.Driver.switch_to.frame("fraLeft")
            print('step2:点击复议工单查询管理')
            self.Driver.find_element_by_id("span500144").click()
        except Exception as e:
            print(e)
            print('capture exception %s' % u"贷前管理-复议工单查询管理按钮未找到")
            self.Driver.get_screenshot_as_file("D:\BusError\error_进入复议工单查询管理页面.png")
        self.Driver.implicitly_wait(20)
        #输入zidonghuaer客服建立的工单编号
        try:
            self.Driver.switch_to.default_content()
            self.Driver.switch_to.frame("fraMain")
            print("   输入zidonghuaer客服建立的工单编号")
            self.Driver.find_element_by_xpath("/html/body/form/div[2]/ul/li[1]/label/input").send_keys(self.FuYiGongDanChaXun_Other_APP_ID)
        except Exception as e:
            print(e)
            print('capture exception %s' % u"贷前管理-复议工单查询管理-录入工单编号文本框未找到")
            self.Driver.get_screenshot_as_file("D:\BusError\error_复议工单查询管理_工单编号文本框页面.png")
        self.Driver.implicitly_wait(20)
        #复议工单查询管理-查询按钮
        try:
            self.Driver.switch_to.default_content()
            self.Driver.switch_to.frame("fraMain")
            print("step3:点击复议工单查询管理-查询按钮")
            self.Driver.find_element_by_xpath("/html/body/form/div[2]/ul/li[4]/button[2]").click()
        except Exception as e:
            print(e)
            print('capture exception %s' % u"贷前管理-复议工单查询管理-查询按钮未找到")
            self.Driver.get_screenshot_as_file("D:\BusError\error_复议工单查询管理_查询按钮页面.png")
        self.Driver.implicitly_wait(20)
        #复议工单查询管理-获取工单编号
        try:
            self.Driver.switch_to.default_content()
            self.Driver.switch_to.frame("fraMain")
            print("    获取查询结果....")
            self.FYGDCXGL_APP_ID=self.Driver.find_element_by_xpath("/html/body/form/table/tbody").text
            return self.FYGDCXGL_APP_ID
        except Exception as e:
            print(e)
            print('capture exception %s' % u"贷前管理-复议工单查询管理-查询结果未获取到")
            self.Driver.get_screenshot_as_file("D:\BusError\error_复议工单查询管理_获取工单编号页面.png")
        self.Driver.implicitly_wait(20)

         #点击主页按钮，返回到主页
        print("=========================================")
        try:
            self.Driver.switch_to.default_content()
            self.Driver.switch_to.frame("fraLeft")
            print('点击主页按钮，返回主页')
            self.Driver.find_element_by_xpath(".//*[@id='main']/div[2]/a").click()
        except Exception as e:
             print(e)
             print('capture exception %s' % u"主页按钮未找到")
             self.Driver.get_screenshot_as_file("D:\BusError\error_返回到主页.png")







    #
