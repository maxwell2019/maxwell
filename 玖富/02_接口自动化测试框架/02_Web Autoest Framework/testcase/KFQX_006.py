# coding = utf-8
#import HTMLTestRunner
import time
import unittest
from selenium import webdriver
from Sharelibs import login, exitlogin
from sshtunnel import SSHTunnelForwarder
import mysql.connector
from Tools import TestobjectReadWriter,Connect_db
from selenium.webdriver.support.select import Select


class TestWebCase:
    def __init__(self,driver,testcase_file,test_sheet):
        self.driver = driver
        self.testcase_file = testcase_file
        self.test_sheet = test_sheet
        print('进入测试case')
        self.server, self.conn = self.login_mysql()
        print("数据库已连接")
        self.verificationErrors = []
        self.accept_next_alert = True

    #读取用户信息
    def get_login_info(self):
        print('读取登录信息')
        login_info = TestobjectReadWriter.ReadWriter_ecxel(self.testcase_file, self.test_sheet)  # 读写
        row = 23##根据用例写入
        col = 'G'##根据用例写入
        Login_info = login_info.read_excel_logininfo(row, col)
        self.Login_username = Login_info[0]
        self.Login_password = Login_info[1]
        print(self.Login_username, self.Login_password)

    #登录数据库
    def login_mysql(self):
        db = Connect_db.GetDB()
        self.conn = db.conn_mysql()
        return self.conn

    # 准备数据
    def pre_data(self):
        self.customer_name = u"畜牧贷部门一"
        self.cert_id = '370902198509085257'
        self.inst_code = '11010319'
        cursor = self.conn.cursor()
        print("查询用户的工单数")
        cursor.execute(
            # "SELECT COUNT(*) from t_lon_application where cert_id='%s'" % self.cert_id)
            "SELECT COUNT(*) from t_lon_application where cert_id='%s' and inst_code = '%s'" %(self.cert_id, self.inst_code))
        result_set = cursor.fetchall()
        pre_num = result_set[0][0]
        print("录单前此用户工单数为%s" % result_set[0][0])
        # 插入此用户其他机构的工单
        # print("创建此用户在外部机构1的工单")
        # cursor.execute("INSERT INTO `pre_loan_db`.`t_lon_application` VALUES ('', NULL, NULL, 'JFB', NULL, 'F0200', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '110852', '3', NULL, NULL, NULL, NULL, '1000', NULL, '668810', '刘纯', NULL, NULL, NULL, 'B1301', '310105198909070040', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', NULL, NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '104', '10', '及时雨', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'F4101', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'BasicOrderProcess:2:17524', NULL, 'B1701', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'F3501', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '1', '1', '1', NULL, NULL, '1', '1', '0', NULL, '/bizpage/loanbefore/instancejsp/recorder/20161014_1552_record_104.jsp', '/bizpage/loanbefore/instancejsp/check/20161014_1552_check_104.jsp', '/bizpage/loanbefore/instancejsp/show/20161014_1552_show_104.jsp', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'zidonghuawbyi', '2016-12-15 14:46:51', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'N8701', 'N8701', '0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2016-12-15 14:46:51', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '100022611410', NULL, NULL, NULL, NULL, NULL, 'C', 'N8702', '1', NULL, NULL, NULL, 'B154001', NULL, NULL, 'c2103bd7-759c-477c-ab15-810d96213d03', NULL, NULL)")
        return pre_num

    def test(self):
        self.get_login_info()
        login_bus = login.Login(self.Login_username,self.Login_password,self.driver)
        self.driver = login_bus.login()
        driver = self.driver
        pre_num = self.pre_data()
        # 进入录单界面

        try:
            print('进入录单页面')
            driver.switch_to.frame("fraLeft")
            print('step1:点击贷前管理')
            driver.find_element_by_id("span500").click()
            self.driver.implicitly_wait(20)
            time.sleep(5)
            print('step2:录单')
            driver.find_element_by_id("span500095").click()
            driver.implicitly_wait(20)
        except:
            driver.get_screenshot_as_file("E:\9F\error.png")
            print('进入录单页面失败')
            testresult = 'Fail'
            resultreason = "进入录单页面失败"
            return testresult, resultreason

        #开始录单
        try:
            driver.switch_to.default_content()
            driver.switch_to.frame("fraMain")
            print("选择创业贷")
            productId = self.driver.find_element_by_id('productId')
            Select(productId).select_by_value("5")
            print("输入用户名、身份证、借款人类型")
            driver.implicitly_wait(20)
            driver.find_element_by_id("customerName").send_keys(self.customer_name)
            driver.find_element_by_id("certId").send_keys(self.cert_id)
            driver.find_element_by_id("borrowerType")
            #选择个人借款
            driver.find_element_by_xpath("/html/body/form/table/tbody/tr[4]/td[2]/select/option[2]").click()
            driver.implicitly_wait(20)
            #查询
            time.sleep(3)
            driver.find_element_by_xpath(".//*[@id='divCrudButton']/ul/li/input").click()
            # 点击下一步
            time.sleep(3)
            driver.find_element_by_xpath(".//*[@id='improveCustem']").click()
            driver.implicitly_wait(20)
            driver.switch_to.default_content()
            driver.switch_to.frame("fraMain")
            #暂存
            driver.find_element_by_xpath("/html/body/div[2]/ul/li[3]/input").click()
            #确定
            driver.find_element_by_xpath("/html/body/div[5]/div[2]/div[3]/a").click()
            driver.implicitly_wait(20)
        except:
            driver.get_screenshot_as_file("E:\9F\error_录单失败.png")
            print('录单失败')
            testresult = 'Fail'
            resultreason = "录单失败"
            return testresult, resultreason

        #查询数据库是否录单成功
        cursor = self.conn.cursor()
        cursor.execute(
            "SELECT COUNT(*) from t_lon_application where cert_id='%s' and inst_code = '%s'" %(self.cert_id, self.inst_code))
        result_set = cursor.fetchall()
        new_num = result_set[0][0]
        print(new_num)
        print("录单后工单总数为：%s" %new_num)
        if(new_num == (pre_num+1)):
            print("录单成功")
        else:
            print("验证录单失败")
            testresult = 'Fail'
            resultreason = "验证录单失败"
            return testresult,resultreason
        #验证结果
        appidnum,print_result,print_reason = self.result_confirm(new_num)
        if(print_result == 'Pass'):
            print("Delete the test data..............")
            cursor.execute("DELETE from t_lon_application where app_id ='%s'" %appidnum)
            self.conn.commit()
            cursor.close()
            self.conn.close()
            self.server.stop()
            return print_result, print_reason
        else:
            return print_result, print_reason
        exit_login = exitlogin.Exitlogin(self.driver)
        exit_login.exitlogin()

    def result_confirm(self, new_num):
        # 数据库验证机构号
        cursor = self.conn.cursor()
        cursor.execute(
            "SELECT APP_ID,INST_CODE from t_lon_application where cert_id='%s' ORDER BY CREATE_TIME DESC LIMIT 1" % self.cert_id)
        result_set = cursor.fetchall()
        app_id = result_set[0][0]
        inst_code = result_set[0][1]
        print("工单号为：%s,机构代码为：%s" % (app_id, inst_code))

        if inst_code != self.inst_code:
            print('Fail')
            testresult = 'Fail'
            resultreason = "数据库验证机构号失败"
            return testresult, resultreason

        testresult,resultreason = self.query(app_id)
        return app_id,testresult,resultreason

    def query(self, app_id):
        # 工单查询（客服）验证
        driver = self.driver
        try:
            print("进入工单查询（客服）界面")
            driver.switch_to.default_content()
            driver.switch_to.frame("fraLeft")
            driver.find_element_by_id("span500031").click()
            print("进入录单查询界面")
            driver.implicitly_wait(20)
        except:
            driver.get_screenshot_as_file("E:\9F\error.png")
            print('进入录单页面失败')
            testresult = 'Fail'
            resultreason = "进入录单页面失败"
            return testresult, resultreason
        # 根据app_id查询
        try:
            print("根据app_id查询")
            driver.switch_to.default_content()
            driver.switch_to.frame("fraMain")
            driver.find_element_by_id("appId").send_keys(app_id)
            driver.implicitly_wait(20)
            driver.find_element_by_id("btnListQuery").click()
            driver.implicitly_wait(20)
            # time.sleep(10)
        except:
            driver.get_screenshot_as_file("E:\9F\error.png")
            print('录单查询失败')
            testresult = 'Fail'
            resultreason = "录单查询失败"
            return testresult, resultreason

        print("获取产品名称")
        try:
            id = driver.find_element_by_xpath("/html/body/form/table/tbody/tr/td[6]").text
            print(id)
            if (id == u'创业贷'):
                testresult = 'Pass'
                resultreason = "验证通过"
            else:
                testresult = 'Fail'
            print("获取产品：Pass or Fail %s" % testresult)
            return testresult, resultreason
        except:
            print('Fail')
            testresult = 'Fail'
            resultreason = "查询结果验证失败"
            return testresult, resultreason