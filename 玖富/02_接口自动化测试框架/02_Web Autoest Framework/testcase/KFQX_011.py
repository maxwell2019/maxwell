# coding = utf-8
import time,os
import unittest
from selenium import webdriver
from Sharelibs import login,exitlogin
from sshtunnel import SSHTunnelForwarder
import mysql.connector
from Tools import TestobjectReadWriter
from Tools import Connect_db

class TestWebCase:
    def __init__(self,driver,testcase_file,test_sheet):
        self.driver = driver
        self.testcase_file = testcase_file
        self.test_sheet = test_sheet
        print('进入测试case')
        self.server, self.conn = self.login_mysql()
        print("数据库已连接")
        self.verificationErrors = []
        self.accept_next_alert = True
        # 创建截图文件夹
        screenshotDir = os.path.abspath('./testResult/screenshopt/') + '\\KFQX_011'
        if not os.path.exists(screenshotDir):
            os.mkdir(screenshotDir)

    def screenShot(self,screenshotContent):
        curTime = time.strftime("%Y%m%d%H%M", time.localtime())
        screenshotPath = os.path.abspath('./testResult/screenshopt/') + '\\KFQX_011\\' + screenshotContent + curTime +  '.png'
        print(screenshotPath)
        self.driver.get_screenshot_as_file(screenshotPath)

    #读取用户信息
    def get_login_info(self):
        print('读取登录信息')
        login_info = TestobjectReadWriter.ReadWriter_ecxel(self.testcase_file, self.test_sheet)  # 读写
        row = 43  ##根据用例写入
        col = 'G'  ##根据用例写入
        Login_info = login_info.read_excel_logininfo(row, col)
        self.Login_username = Login_info[0]
        self.Login_password = Login_info[1]
        print(self.Login_username, self.Login_password)

    #登录数据库
    def login_mysql(self):
        connect = Connect_db.GetDB()
        server, conn = connect.conn_mysql()
        return server, conn

    def test(self):
        self.get_login_info()
        login_bus = login.Login(self.Login_username,self.Login_password,self.driver)
        login_bus.login()
        #验证结果
        testResult,reason = self.query()
        self.server.stop()
        exit_login = exitlogin.Exitlogin(self.driver)
        exit_login.exitlogin()
        return testResult,reason

    def query(self):
        # 工单查询（客服）验证
        driver = self.driver
        try:
            print("进入工单查询（客服）界面")
            driver.switch_to.frame("fraLeft")
            print("进入贷前管理")
            driver.find_element_by_id("span500").click()
            driver.implicitly_wait(30)
            print("进入录单查询界面")
            driver.find_element_by_id("span500031").click()
            driver.implicitly_wait(20)
            # time.sleep(5)
        except:
            self.screenShot("进入工单查询（客服）异常")
            reason = u'进入工单查询（客服）异常'
            print('Fail:进入工单查询（客服）异常')
            testResult = 'Fail'
            return testResult, reason

        # 查询所有工单
        try:
            print("查询")
            driver.switch_to.default_content()
            driver.switch_to.frame("fraMain")
            driver.find_element_by_id("btnListQuery").click()
            driver.implicitly_wait(20)
            # time.sleep(10)
        except:
            self.screenShot("工单查询（客服）查询失败")
            reason = u'工单查询（客服）查询失败'
            print('Fail:进入工单查询（客服）查询失败')
            testResult = 'Fail'
            return testResult, reason

        #获取页面工单数和页数
        print("获取工单总数和页数")
        try:
            driver.find_element_by_id('chaxun').click()
            driver.implicitly_wait(20)
            text = driver.find_element_by_id('totalPage').text
        except:
            self.screenShot("工单查询（客服）获取总数失败")
            reason = u'工单查询（客服）获取总数失败'
            print('Fail:进入工单查询（客服）获取总数失败')
            testResult = 'Fail'
            return testResult, reason
        #获取字符串中的数字
        print(text)
        #获取工单条数
        nums = int(text.split("条")[0].split("共")[1])
        #获取页数
        pages = int(text.split("，")[1].split("页")[0])
        print("工单数为%d" %nums)
        print("页数为%d" %pages)

        #验证数据库中总数与页面显示一致
        cursor = self.conn.cursor()
        #查询此用户的产品，渠道，机构下的工单数
        cursor.execute(
            "SELECT count(*) from t_lon_application where SALE_CHANNEL='1049' and INST_CODE='110851' and PRODUCT_ID='246'")
        result_set = cursor.fetchall()
        sql_num = result_set[0][0]

        if (nums != sql_num):
            print('Fail')
            testResult = 'Fail'
            reason = u'工单查询（客服）查询获取到的总数与数据库查询总数不一致'
            return testResult,reason
        else:
            print("工单总数验证成功")

        orders = 0
        #逐个工单验证渠道是否正确
        for page in range(pages):
            for num in range(15):
                if(orders<nums):
                    try:
                        text = driver.find_element_by_xpath('/html/body/form/table/tbody/tr[%s]/td[5]' %(num+1)).text
                    except:
                        print("Fail:验证查询异常")
                        testResult = 'Fail'
                        reason = u'验证查询异常'
                        return testResult, reason

                    orders += 1
                    if(text!=u'天使'):
                        print("Fail:验证失败,渠道名称错误")
                        testResult = 'Fail'
                        reason=u'渠道名称错误'
                        return testResult,reason
                    else:
                        continue
                else:
                    break
            print("第%d页验证成功" % (page + 1))
                # print("order is %d" %orders)
            if((page+1) != pages):
                if(page == 0):
                    try:
                        driver.find_element_by_xpath('/html/body/form/div[4]/table/tbody/tr[2]/td[3]/a').click()
                        driver.implicitly_wait(20)
                    except:
                        print("Fail:查询翻页异常")
                        testResult = 'Fail'
                        reason=u'查询翻页异常'
                        return testResult,reason
                else:
                    try:
                        driver.find_element_by_xpath('/html/body/form/div[4]/table/tbody/tr[2]/td[3]/a[2]').click()
                        driver.implicitly_wait(20)
                    except:
                        print("Fail:查询翻页异常")
                        testResult = 'Fail'
                        reason=u'查询翻页异常'
                        return testResult,reason

        print("逐条验证成功,共验证%d条" % orders)
        print('Pass')
        testResult = 'Pass'
        reason = ''
        return testResult,reason