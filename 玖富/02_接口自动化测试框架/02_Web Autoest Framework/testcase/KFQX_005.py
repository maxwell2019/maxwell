# coding = utf-8
#import HTMLTestRunner
from Sharelibs import login
import time
import unittest
from selenium import webdriver
# from Sharelibs import login
from sshtunnel import SSHTunnelForwarder
import mysql.connector
from Tools import TestobjectReadWriter
from selenium.webdriver.support.select import Select

class TestWebCase:
    def __init__(self,driver,testcase_file,test_sheet):
        self.driver = driver
        self.testcase_file = testcase_file
        self.test_sheet = test_sheet
        print('进入测试case')
        self.server, self.conn = self.login_mysql()
        print("数据库已连接")
        self.verificationErrors = []
        self.accept_next_alert = True

    #读取用户信息
    def get_login_info(self):
        print('读取登录信息')
        login_info = TestobjectReadWriter.ReadWriter_ecxel(self.testcase_file, self.test_sheet)  # 读写
        print('000000000000000')
        row = 19##根据用例写入
        col = 'G'##根据用例写入
        Login_info = login_info.read_excel_logininfo(row, col)
        self.Login_username = Login_info[0]
        self.Login_password = Login_info[1]
        print(self.Login_username, self.Login_password)

    #登录数据库
    def login_mysql(self):
        print("login mysql")
        server = SSHTunnelForwarder(
            ssh_address_or_host=('182.92.106.154', 22),  # 指定ssh登录的跳转机的address
            ssh_username='read',  # 跳转机的用户
            ssh_password='Jff111',  # 跳转机的密码
            local_bind_address=('127.0.0.1', 10000),  # 本地绑定的端口
            remote_bind_address=('rds9jf18bkcjtpenigshn.mysql.rds.aliyuncs.com', 3306))  # 远程绑定的端口

        server.start()

        conn = mysql.connector.connect(host='127.0.0.1',  # 此处必须是是127.0.0.1
                                       port=10000,  # 本地绑定的端口
                                       user='pre_loan01',
                                       passwd='2izkcoedKj',
                                       db='pre_loan_db')
        # 启动以后，操作本地的10000端口相当于操作10.10.11.102的3306端口
        return server, conn

    # 准备数据
    def pre_data(self):
        self.customer_name = u"史钢军"
        self.cert_id = '620111198508300431'
        self.inst_code = '11040211'
        cursor = self.conn.cursor()
        print("创建此用户在外部机构1的工单")
        cursor.execute(
            "INSERT INTO `pre_loan_db`.`t_lon_application` (`APP_ID`, `IPC_ID`, `IPC_APP_ID`, `ORG_CODE`, `APP_CODE`, `APP_STATUS`, `END_REASON`, `UP_BACK_REASON`, `UP_BACK_REASON_REMARK`, `IS_SANBIAO`, `APP_KEY`, `COOPERATE_CODE`, `AREA_CODE`, `INST_CODE`, `INST_STYLE`, `GROUP_ID`, `GROUP_NAME`, `CHECK_TEAM_ID`, `GROUP_TEAM_ID`, `SALE_CHANNEL`, `CUSTOMER_SOURCE`, `CUSTOMER_ID`, `CUSTOMER_NAME`, `CUSTOMER_SEX`, `CUSTOMER_LEVEL`, `DEGREE`, `CERT_TYPE`, `CERT_ID`, `MARRY`, `PHONE`, `E_MAIL`, `QQ`, `STUDENT_NUM`, `WEIXIN_NUM`, `WEIBO_NUM`, `LIVIING_TEYP`, `HOUSE_LOCAL`, `PEOPLE_LOCAL`, `REGIST_ADDRESS`, `REGIST_POSTCODE`, `LIVE_ADDRESS`, `LIVE_POSTCODE`, `LIVE_PHONE`, `POST_ADDRESS`, `PERSONAL_REMARK`, `RECEIVE_BANK_CARD`, `RECEIVE_OPEN`, `RECEIVE_NAME`, `RECEIVE_BRANCH`, `RECEIVE_PROVINCE`, `RECEIVE_COUNTRY`, `RECEIVE_COUNTRY_CODE`, `REPAY_BANK_CARD`, `REPAY_OPEN`, `REPAY_NAME`, `REPAY_BRANCH`, `IS_OPEN_CARD`, `REPAY_ACCOUNT_NAME`, `REPAY_ACCOUNT`, `PAYMENT_TYPE`, `IS_CARD`, `HAVE_PLAN`, `RESIDE_DATE`, `TO_CITY_DATE`, `PERSONAL_ASSET_AMT`, `COMPANY`, `BEGIN_COMPANY_DATE`, `COMPANY_TYPE`, `COMPANY_ADDRESS`, `COMPANY_PHONE`, `COMPANY_POSTCODE`, `INTUSTRY`, `DUTY`, `SALARY_AMT`, `BUSINESS_AMT`, `SALARY_DAY`, `OTHER_INCOME_AMT`, `COMPANY_REMARK`, `PRODUCT_ID`, `PRODUCT_TYPE_ID`, `PRODUCT_NAME`, `CHARGE_TYPE`, `APPAY_AMT`, `APPAY_DATE`, `SIGN_DATE`, `LOAN_DATE`, `LOAN_PURPOSE`, `LOAN_PURPOSE_OTHER`, `TIME_LIMIT`, `CHARGE_RULE`, `HAND_CHARGE_RATE`, `INTEREST_RULE`, `YEAR_RATE`, `MONTH_RATE`, `PUNISH_RATE`, `ORIGIN_RATIO`, `ADJUST_RATIO`, `EXCEED_RATE`, `DISCOUNT`, `REPAY_DAY`, `SERVICE_FEE_AMT`, `RISK_AMT`, `RISK_AMT_RATE`, `ADVISORY_AMT`, `SERVICE_AMT`, `ADVISORY_SERVICE_AMT`, `ACCOUNT_MNG_AMT`, `DEDUCTED_AMOUNT`, `DEPOSIT_AMT`, `DEPOSIT_AMT_RATE`, `SCORE_TEMPLETE_ID`, `FIRST_REPAY_DATE`, `MONTH_REPAY_LIMIT`, `LAST_REPAY_AMT`, `HAVE_CREDIT`, `CUSTOMER_TYPE`, `CUSTOMER_PROPERTY`, `FIRM_TYPE`, `HAVE_LICENSE`, `MONTH_BASIC_INCOME_AMT`, `MONTH_OTHER_INCOME_AMT`, `INCOME_TYPE`, `LAST3_LIST`, `RECENT_PRIVATE_LIST`, `RECENT_PUBLIC_LIST`, `AVERAGE_LIST_AMT`, `AVERAGE_INCOME_AMT`, `JOB_PROVE`, `JOB_PROVE_URL`, `INDUSTRY_REGIST`, `HOUSE_CAR`, `IS_PROPERTY`, `PROPERTY_TYPE`, `IS_LOAN`, `LOAN_TYPE`, `MONTH_REPAY_AMT`, `PROPERTY_REMARK`, `PROCESS_TEMPLETE_ID`, `PROCESS_INSTANCE_ID`, `APP_LOAN_TYPE`, `CONTRACT_TEMPLATE`, `CONTRACT_CODE`, `CONTRACT_STATUS`, `CONTRACT_URL`, `REQUEST_DATE`, `FULL_DATE`, `PLAN_REPAY_DATE`, `CONTRACT_AMT`, `APPROVE_AMT`, `APPROVE_LIMIT`, `SUGGEST_REMARK`, `REPAY_SUM_AMT`, `REPAY_SUM_PRINCIPAL`, `REMAIN_SUM_PRINCIPAL`, `PUNISH_TYPE`, `HAVA_EXCEED`, `EXCEED_AMT`, `EXCEED_MANAGER_AMT`, `PUNISH_AMT`, `VIOLATE_AMT`, `REPAY_EXCEED_MANAGER_AMT`, `REPAY_PUNISH_AMT`, `REPAY_VIOLATE_AMT`, `REPAY_STATUS`, `ZPSWIFT_BATCH_NO`, `ZPSWIFT_ID`, `ALONE_SWIFT_NO`, `PRIOR`, `CREDITOR_RIGHTS`, `BUSINESS_TIME`, `REGISTED_TIME`, `WORK_TIME`, `YNHOUSE_LOAN_OVERDUE`, `IS_MATCH`, `TRANSFER_CHANNEL`, `IS_SIGN_CONTACT`, `IS_CAL_TOTAL_AMOUNT`, `IS_PAY_PLAN`, `LOAN_TARGET`, `INST_LOAN_CARD`, `INST_LOAN_CARD_NAME`, `REPAYMENT_INITIATOR`, `IS_REPAYMENT`, `IS_SUPPORT_DEDUCTION`, `old_app_id`, `RECORD_URL`, `CHECK_URL`, `REVIEW_URL`, `EXCEED_BEGIN_DATE`, `FIRST_STAGE_REPAY_AMT`, `SECOND_STAGE_REPAY_AMT`, `EXTEND_VC1`, `EXTEND_VC2`, `EXTEND_VC3`, `EXTEND_VC4`, `EXTEND_VC5`, `EXTEND_VC6`, `EXTEND_VC7`, `EXTEND_VC8`, `EXTEND_VC9`, `EXTEND_VC10`, `EXTEND_TIME1`, `EXTEND_TIME2`, `EXTEND_TIME3`, `EXTEND_TIME4`, `EXTEND_TIME5`, `EXTEND_FL1`, `EXTEND_FL2`, `EXTEND_FL3`, `EXTEND_FL4`, `EXTEND_FL5`, `CREATOR`, `CREATE_TIME`, `MENDER`, `MEND_TIME`, `REMARK`, `FILE_UP_END_DATE`, `EXPECT_CAR_AMOUNT`, `MONTH_NET_PROFIT`, `LOCAL_RESIDENCE`, `LOCAL_WORK_EXPERIENCE`, `MORTGAGOR_NAME`, `MORTGAGOR_ACCOUNT`, `CAR_REMARK`, `OLDASSET_OWNER`, `IS_SEND_MESSAGE`, `IS_OFFER`, `IS_RENEW`, `RENEW_MANAGER`, `RENEW_MANAGER_ID`, `SCHOOL`, `JOB_NATURE`, `JOBS`, `LOAN_AMOUNT_OF_MR`, `YSX_ID`, `IS_HAS_COMMON_BORROWER`, `OPERATION_VERIFY_BACK_FLAG`, `update_time`, `PAWN_CATEGORY`, `REGIST_ADDRESS_PROVINCE`, `REGIST_ADDRESS_CITY`, `REGIST_ADDRESS_DISTRICT`, `COMPANY_ADDRESS_PROVINCE`, `COMPANY_ADDRESS_CITY`, `COMPANY_ADDRESS_DISTRICT`, `HOUSEHOLD_REGISTER`, `NATION`, `RULE_ID`, `INVESTIGATE_WAY`, `CHANNEL_ID`, `ENTER_ID`, `LOAN_CODE`, `RENEW_TYPE`, `career`, `ORGANIZATION_CODE`, `ACCOUNT_TYPE`, `TRUSTEE_TYPE`, `OVER_DUE_LEVEL`, `IS_ASSURE`, `MANAGER_TIME_LENGTH`, `CONFIG_VERSION_ID`, `SYS_APPROVE_SUGGESTAMT`, `REDUCE_PERIODS`, `BORROWER_TYPE`, `YSX_PZSC_ID`, `REPAY_CARD_BIND_ID`, `UUM_USER_ID`, `UUM_CUST_NO`, `DRAWN_AMT`) VALUES ('', NULL, NULL, 'JFB', NULL, 'F0200', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '11040211', '', NULL, NULL, NULL, NULL, '1000', NULL, '646865', '史钢军', 'N0201', NULL, 'B0304', 'B1301', '620111198508300431', 'B0501', '13521987022', '534678022@qq.com', '', NULL, NULL, NULL, NULL, NULL, NULL, '', '', '北京市顺义区', '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', NULL, NULL, NULL, '1', NULL, NULL, '', NULL, '', '', '', '', '', '', 'B1025', 'B2915', '0.00', '0.00', '0', '0.00', NULL, '244', '10', '及时雨2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'F4101', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'BasicOrderProcess:2:17524', NULL, 'B1701', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'F3501', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '1', '1', '1', NULL, NULL, '1', '1', '0', NULL, '/bizpage/loanbefore/instancejsp/recorder/20160930_1400_record_244.jsp', '/bizpage/loanbefore/instancejsp/check/20160930_1400_check_244.jsp', '/bizpage/loanbefore/instancejsp/show/20160930_1400_show_244.jsp', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'zidonghuawhfd', '2016-12-19 18:28:03', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'N8701', 'N8701', '0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2016-12-19 18:28:03', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '100022611838', NULL, NULL, NULL, NULL, NULL, 'C', 'N8702', '1', NULL, NULL, NULL, 'B154001', NULL, NULL, '37ff27e1-a800-427f-8ae5-2649e1937d1b', NULL, NULL);")
        self.conn.commit()
        print("查询用户的工单数")
        cursor.execute(
            "SELECT COUNT(*) from t_lon_application where cert_id='%s'" % self.cert_id)
        result_set = cursor.fetchall()
        pre_num = result_set[0][0]


        print("录单前此用户工单数为%s" % result_set[0][0])
        # 插入此用户其他机构的工单

        return pre_num

    def test(self):
        self.get_login_info()
        login_bus = login.Login(self.Login_username,self.Login_password,self.driver)
        login_bus.login()
        driver = self.driver
        pre_num = self.pre_data()

        # 进入录单界面
        try:
            print('进入录单页面')
            driver.switch_to.frame("fraLeft")
            print('step1:点击贷前管理')
            driver.find_element_by_id("span500").click()
            self.driver.implicitly_wait(20)
            time.sleep(5)
            print('step2:录单')
            # driver.find_element_by_id("span500095").waitForElementPresent(u'录单')
            # driver.find_element_by_id("span500095").verifyText(u'录单')
            driver.find_element_by_id("span500095").click()
            driver.implicitly_wait(20)
        except:
            driver.get_screenshot_as_file("E:\9F\error_进入录单页.png")
            print('进入录单页面失败')
            username = ''
            return(username)

        #开始录单
        try:
            driver.switch_to.default_content()
            driver.switch_to.frame("fraMain")
            print("及时雨2")
            productId = self.driver.find_element_by_id('productId')
            Select(productId).select_by_value("244")
            print("输入用户名、身份证、借款人类型")
            driver.implicitly_wait(20)
            driver.find_element_by_id("customerName").send_keys(self.customer_name)
            driver.find_element_by_id("certId").send_keys(self.cert_id)
            driver.find_element_by_id("borrowerType")
            #选择个人借款
            driver.find_element_by_xpath("/html/body/form/table/tbody/tr[4]/td[2]/select/option[2]").click()
            driver.implicitly_wait(20)
            #查询
            driver.find_element_by_xpath("/html/body/form/div[2]/ul/li/input").click()
            driver.implicitly_wait(20)
            time.sleep(5)
            #下一步
            print("下一步")
            driver.find_element_by_id("improveCustem").click()
            driver.implicitly_wait(20)
            driver.switch_to.default_content()
            driver.switch_to.frame("fraMain")
            #暂存
            driver.find_element_by_xpath("/html/body/div[2]/ul/li[3]/input").click()
            #确定
            driver.find_element_by_xpath("/html/body/div[5]/div[2]/div[3]/a").click()
            driver.implicitly_wait(20)
        except:
            driver.get_screenshot_as_file("E:\9F\error_录单失败.png")
            print('录单失败')
            username = ''
            return(username)

        #查询数据库是否录单成功
        cursor = self.conn.cursor()
        cursor.execute(
            "SELECT COUNT(*) from t_lon_application where cert_id='%s'" %self.cert_id)
        result_set = cursor.fetchall()
        new_num = result_set[0][0]
        print(new_num)
        print("录单后工单总数为：%s" %new_num)
        if(new_num == (pre_num+1)):
            print("录单成功")
        else:
            print("验证录单失败")
            username = ''
            return (username)
        #验证结果
        # self.driver = driver
        print("准备结果验证------------------")
        self.result_confirm(new_num)
        self.server.stop()

    def result_confirm(self, new_num):
        # 数据库验证机构号
        cursor = self.conn.cursor()
        cursor.execute(
            "SELECT APP_ID,INST_CODE from t_lon_application where cert_id='%s' ORDER BY CREATE_TIME DESC LIMIT 1" % self.cert_id)
        result_set = cursor.fetchall()
        app_id = result_set[0][0]
        inst_code = result_set[0][1]
        print("工单号为：%s,机构代码为：%s" % (app_id, inst_code))

        if inst_code != inst_code:
            print('Fail', end='')
            testresult = 'Fail'

        testresult = self.query(app_id)

        return testresult

    def query(self, app_id):
        # 工单查询（客服）验证
        driver = self.driver
        try:
            print("进入工单查询（客服）界面")
            driver.switch_to.default_content()
            driver.switch_to.frame("fraLeft")
            driver.find_element_by_id("span500031").click()
            print("进入录单查询界面")
            driver.implicitly_wait(20)
            # time.sleep(5)
        except:
            driver.get_screenshot_as_file("E:\9F\error_进入工单查询（客服）失败.png")
            print('进入录单页面失败')
            username = ''
            return (username)

        # 根据app_id查询
        try:
            print("根据app_id查询")
            driver.switch_to.default_content()
            driver.switch_to.frame("fraMain")
            driver.find_element_by_id("appId").send_keys(app_id)
            driver.implicitly_wait(20)
            driver.find_element_by_id("btnListQuery").click()
            driver.implicitly_wait(20)
            # time.sleep(10)
        except:
            driver.get_screenshot_as_file("E:\9F\error_查询失败.png")
            print('录单查询失败')
            username = ''
            return (username)

        print("获取产品名称")
        try:
            print(driver.find_element_by_xpath("/html/body/form/table/tbody").text)
            id = driver.find_element_by_xpath("/html/body/form/table/tbody/tr/td[6]").text
            print(id)
            if (id != u'及时雨2'):
                print('Fail', end='')
                testresult = 'Fail'
        except:
            print('Fail', end='')
            testresult = 'Fail'

        # 验证其他机构录单不能查询到
        # 查询其他机构的app_id
        cursor = self.conn.cursor()
        cursor.execute(
            "SELECT APP_ID from t_lon_application where cert_id='310105198909070040' and INST_CODE ='11'  ORDER BY CREATE_TIME DESC LIMIT 1")
            # "SELECT APP_ID from t_lon_application where cert_id='%s' and INST_CODE ='11040211'  ORDER BY CREATE_TIME DESC LIMIT 1" % self.cert_id)
        result_set = cursor.fetchall()
        other_app_id = result_set[0][0]
        print(other_app_id)

        try:
            print("根据app_id查询其他机构工单")
            driver.switch_to.default_content()
            driver.switch_to.frame("fraMain")
            driver.find_element_by_id("appId").clear()
            driver.find_element_by_id("appId").send_keys(other_app_id)
            driver.implicitly_wait(20)
            driver.find_element_by_id("btnListQuery").click()
            driver.implicitly_wait(20)
            # time.sleep(10)
        except:
            driver.get_screenshot_as_file("E:\9F\error_其他机构工单查询失败.png")
            print('其他机构工单查询失败')
            username = ''
            return (username)
        #没有查询到pass，其他fail
        try:
            id = driver.find_element_by_xpath("/html/body/form/table/tbody").text
            if id == "":
                print('Pass', end='')
                testresult = 'Pass'
            else:
                print('Fail', end='')
                testresult = 'Fail'
        except:
            print("异常")
            print('Fail', end='')
            testresult = 'Fail'
        return testresult


