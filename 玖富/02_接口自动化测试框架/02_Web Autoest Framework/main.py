# -*- coding:utf-8 -*-

__author__ = 'fubo'
from selenium import webdriver
from Tools import Runcase,Connect_db
from config import globalconfig
from Sharelibs import login
import unittest
from Tools import TestobjectReadWriter
from rizhi import log_config as a
import logging


class testmain(unittest.TestCase):

    @classmethod
    def setUpClass(self):
        print('start...')
        self.driver = webdriver.Firefox()

    def test(self):
        log_msg = a.RiZhi()
        log_msg.log_def()
        logging.info('This is info')
        #print('login...')
        #self.login = login.Login(self.driver)
        #self.Ln_driver = self.login.login()

        self.config = globalconfig.Global()
        #
        case_name = self.config.get_case_name()
        logging.info('testmode:%s' % case_name)
        run_mode = self.config.get_run_mode()
        logging.info('testmode:%s' %run_mode)
        run_case_list = self.config.get_run_case_list()
        logging.info('testcase:%s' %run_case_list)
        #...
        testcase_file = self.config.get_testcase_file()
        logging.info('测试文件路径：%s'%testcase_file)
        test_sheet = self.config.get_testsheet()
        logging.info('测试文件_sheet:%s'%test_sheet)
        #getcon = Connect_db.GetDB()
        #getcon.conn_mysql()
        #case_runner = Runcase.RunCase(case_name,run_mode,run_case_list,testcase_file,test_sheet,self.Ln_driver)

        case_runner = Runcase.RunCase(case_name, run_mode, run_case_list, testcase_file, test_sheet,self.driver)
        case_list, result_info_list = case_runner.run_case()
        result_W = TestobjectReadWriter.ReadWriter_ecxel(testcase_file,test_sheet)
        result_W.writer_excel_testresult(case_list,result_info_list)


    @classmethod
    def tearDownClass(self):
        driver = self.driver
        driver.quit()

if __name__ == '__main__':
        unittest.main()
