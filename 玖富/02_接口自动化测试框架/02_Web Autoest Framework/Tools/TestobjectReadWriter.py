# -*- coding:utf-8 -*-
__author__ = '付波'
from openpyxl import load_workbook
from openpyxl import Workbook
from openpyxl.writer.excel import ExcelWriter
import logging

class ReadWriter_ecxel:
    def __init__(self,casefile_path,testsheetname):
        self.casefile_path  = casefile_path
        print(self.casefile_path)
        #self.testsheetmode = testsheetmode
        self.testsheetname = testsheetname
        print(self.testsheetname)

    def writer_excel_testresult(self,case_list,result_info_list):
        try:
            wb = load_workbook(self.casefile_path)
            ew = ExcelWriter(workbook=wb)
            ws_sheet = wb.get_sheet_by_name(self.testsheetname)
            row_sum = len(ws_sheet.rows)
            tmp_case_list = self.read_excel_caseinfo()
            print('excel中读取的caselist',tmp_case_list)
            print('执行caselist:',case_list)
            print('开始回填结果判断')
            for result_info_tmp in  result_info_list:
                for case_id in case_list:
                    for case_id_tmp in tmp_case_list:
                        if case_id_tmp == case_id:
                                for row in range (3,row_sum):
                                    case_id_TM = ws_sheet.cell('D%s' % row).value
                                    if case_id_TM == case_id:
                                        ws_sheet.cell('o%s' % row).value = result_info_tmp
                                        ew.save('test_result' + '.xlsx')
                                        print('%s的结果回填完毕'%case_id)
                                    else:
                                        print('该case未执行，继续往下填写测试结果')
                        else:
                            print('执行case与用例case不匹配，继续查找。。。')
        except Exception as e:
            print("读取error %s" % e)
            #logger.info("%s" % e)

    def read_excel_logininfo(self,row,col):
        try:
            print('获取登录信息')
            wb = load_workbook(self.casefile_path)
            print(wb)
            ws_sheet = wb.get_sheet_by_name(self.testsheetname)
            print(ws_sheet)
            login_username = ws_sheet.cell('%s%s' %(col,row)).value
            print(login_username)
            row +=1
            login_password = ws_sheet.cell('%s%s' % (col, row)).value
            print(login_password)

        except Exception as e:
            print("读取error %s" % e)
            #logger.info("%s" % e)

        return login_username,login_password

    def read_excel_caseinfo(self):
        try:
            wb = load_workbook(self.casefile_path)
            ws_sheet = wb.get_sheet_by_name(self.testsheetname)
        except Exception as e:
            print("打开进入excel error%s" %e)
        try:
            #print(ws_sheet.rows)
            row_sum = len(ws_sheet.rows)
            columns_sum = len(ws_sheet.columns)
            case_list = []
            #print(row_sum,columns_sum)
            for row in range (3,row_sum):
                case_id = ws_sheet.cell('D%s' %row).value
                print(case_id)
                if case_id is None:
                    print('用例标号为空，继续获取')
                else:
                    case_list.append(case_id)
        except Exception as e:
            print('读取excel信息错误 %s' %e)

        return case_list