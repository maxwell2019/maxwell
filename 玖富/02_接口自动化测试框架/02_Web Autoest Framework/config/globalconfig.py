﻿# -*- coding:utf-8 -*-
__author__ = 'fubo'
from config import configrunmode,configtestobject,configdb
import  os

class Global:
    def __init__(self):
        path = os.path.abspath(os.curdir)
        #print(path)
        runmode_config_path = path + '\\' + 'config\\runmode_config.ini'
        runtest_object_path = path + '\\'+'config\\testobject_config.ini'
        db_conn_path = path + '\\' + 'config\\database_config.ini'
        print(db_conn_path)


        self.run_mode_config = configrunmode.ConfigRunMode(runmode_config_path)
        self.run_test_object = configtestobject.TestObject(runtest_object_path)
        self.db_conn = configdb.GetDB(db_conn_path)

    def get_case_name(self):
        return self.run_mode_config.get_case_name()

    def get_run_mode(self):
        return self.run_mode_config.get_run_mode()

    #
    def get_run_case_list(self):
        return self.run_mode_config.get_case_list()

    #
    def get_testcase_file(self):
        return self.run_test_object.get_testcase_file()

    def get_testsheet(self):
        return self.run_test_object.get_testsheet()

    def get_conn(self):
        return self.db_conn.get_conn()