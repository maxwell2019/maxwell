# -*- coding:utf-8 -*-

__author__ = 'fubo'

from getdb import GetDB
from confighttp import ConfigHttp
from configrunmode import ConfigRunMode

class Global:
    def __init__(self):
        self.http = ConfigHttp('../http_conf.ini')

        self.db1 = GetDB('../db_config.ini', 'DATABASE1')
        #self.db2 = GetDB('../db_config.ini', 'DATABASE2')

        # 
        self.run_mode_config = ConfigRunMode('../run_case_config.ini')

    def get_http(self):
        return self.http

    # 
    def get_db1_conn(self):
        return self.db1.get_conn()

    #def get_db2_conn(self):
     #   return self.db2.get_conn()

    # 
    def get_run_mode(self):
        return self.run_mode_config.get_run_mode()

    # 
    def get_run_case_list(self):
        return self.run_mode_config.get_case_list()

    # 
    def clear(self):
        # 
        self.db1.get_conn().close()
        #self.db2.get_conn().close()
