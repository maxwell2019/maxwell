# -*- coding:utf-8 -*-

__author__ = 'fubo'

from pyh import *
import time
import os

class HtmlReport:
    def __init__(self, cursor):
        self.title = 'interface aototestreport_IRDTec'   # 
        self.filename = ''                   # 
        self.time_took = '00:00:00'         # 
        self.success_num = 0                  # 
        self.fail_num = 0                     # 
        self.error_num = 0                    # 
        self.case_total = 0                   # 
        self.cursor = cursor
        
    # 
    def generate_html(self,head, file):
            page = PyH(self.title)
            
            page << h1(head, align='center') # 

            page << p('测试总耗时：' + self.time_took)

            # 
            query = ('SELECT count(case_id) FROM test_result')
            self.cursor.execute(query)
            self.case_total = self.cursor.fetchone()[0]

            # 
            self.cursor.execute('SELECT count(case_id) FROM test_result WHERE result = %s',('Fail',))
            self.fail_num = self.cursor.fetchone()[0]

            # 
            self.cursor.execute('SELECT count(case_id) FROM test_result WHERE result = %s',('Pass',))
            self.success_num = self.cursor.fetchone()[0]

            # 
            self.cursor.execute('SELECT count(case_id) FROM test_result WHERE result = %s',('Error',))
            self.error_num = self.cursor.fetchone()[0]

            page << p('测试用例总数：' + str(self.case_total) + '&nbsp'*10 + '执行通过用例数：' + str(self.success_num) +
                      '&nbsp'*10 + '执行不通过用例数：' + str(self.fail_num) + '&nbsp'*10 +  '执行出错用例数：' + str(self.error_num))
            #  

            tab = table( border='1', cellpadding='1', cellspacing='0', cl='table')
            tab1 = page << tab
            tab1 << tr(td('用例ID', bgcolor='#ABABAB', align='center')
                       + td('请求方法', bgcolor='#ABABAB', align='center')
                       + td('接口名称', bgcolor='#ABABAB', align='center')
                       + td('请求URL', bgcolor='#ABABAB', width='40px', align='center')
                       + td('请求参数/数据', bgcolor='#ABABAB',width='100px', align='center')###
                       + td('测试预期结果', bgcolor='#ABABAB', align='center')
                       + td('测试方法', bgcolor='#ABABAB', align='center')
                       + td('服务器返回code', bgcolor='#ABABAB', align='center')
                       + td('测试结果', bgcolor='#ABABAB', align='center')
                       + td('服务器返回msg', bgcolor='#ABABAB', align='center'))

            # 
            query = ('SELECT case_id, http_method, request_name, request_url,'
                          'request_param, test_expect, test_method, test_desc, result, reason FROM test_result')
            self.cursor.execute(query)
            query_result = self.cursor.fetchall()
        
            for row in query_result:
                if row[8]=='Fail': #
                    tab1<< tr(td(int(row[0]), align='center') + td(row[1]) +
                              td(row[2]) + td(row[3]) +
                              td(row[4]) + td(row[5], align='center') + td(row[6], align='center') +
                              td(row[7], align='center') + td(row[8],bgColor='#ff0000') + td(row[9]))

                else:
                    tab1<< tr(td(int(row[0]), align='center') + td(row[1]) +
                              td(row[2]) + td(row[3]) +
                              td(row[4]) + td(row[5], align='center') + td(row[6], align='center') +
                              td(row[7], align='center') + td(row[8]) + td(row[9]))
                
                
            self._set_result_filename(file)
            page.printOut(self.filename)

            #try:
             #   query = ('DELETE FROM test_result')
             #   self.cursor.execute(query)
              #  self.cursor.execute('commit')
            #except Exception as e:
                # 
             #   print('%s' % e)
              #  self.cursor.execute('rollback')
            self.cursor.close()

    # 
    def _set_result_filename(self, filename):
        self.filename = filename
        #
        if os.path.isdir(self.filename):
            raise IOError("%s must point to a file" % path)
        elif '' == self.filename:
            raise IOError('filename can not be empty')
        else:
            parent_path, ext = os.path.splitext(filename)
            tm = time.strftime('%Y%m%d%H%M%S', time.localtime())
            self.filename = parent_path + '_Daibangbang_'+ tm + ext

    # 
    def set_time_took(self, time):
        self.time_took = time
        return self.time_took
