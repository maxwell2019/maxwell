# -*- coding:utf-8 -*-

__author__ = 'fubo'

import urllib.request
import http.cookiejar
import urllib.parse
import json
import configparser
import ssl

class ConfigHttp:

    def __init__(self, ini_file):
        config = configparser.ConfigParser()

        config.read(ini_file)
        self.host = config['HTTPS']['host'] #http
        self.port = config['HTTPS']['port'] #http
        #self.hosts = config['HTTPS']['host']
        #self.ports = config['HTTPS']['port']
        self.headers = {}  # http 头

        #install cookie
        cj = http.cookiejar.CookieJar()
        opener = urllib.request.build_opener(urllib.request.HTTPCookieProcessor(cj))
        urllib.request.install_opener(opener)
    #http
    #def set_host(self, host):
    #    self.host = host

    #def get_host(self):
    #    return self.host

    #def set_port(self, port):
    #    self.port = port

    #def get_port(self):
    #    return  self.port
    
    #https
    def set_host(self, host):
        self.host = host

    def get_host(self):
        return self.host
    
    def set_port(self, porst):
        self.port = port

    def get_port(self):
        return  self.port

    # 设置http头
    def set_header(self, headers):
        self.headers = headers

    def test_httpspost(self, url, params):
        #params = urllib.parse.urlencode(eval(params))  # 将参数转为url编码字符串
        https_sslv3_handler = urllib.request.HTTPSHandler(context=ssl.SSLContext(ssl.PROTOCOL_SSLv3))##ssl https方式
        opener = urllib.request.build_opener(https_sslv3_handler)
        urllib.request.install_opener(opener)
        params_temp = params
        url = 'https://' + self.host + ':' + str(self.port)  + url +params_temp ###贷帮帮https get
        request = urllib.request.Request(url, headers=self.headers)
        
        try:
            response = urllib.request.urlopen(request)
            response = response.read().decode('utf-8')  ## decode函数对获取的字节数据进行解码
            json_response = json.loads(response)  # 将返回数据转为json格式的数据
            return json_response
        except Exception as e:
            print('%s' % e)
            return {}

    def post(self, url, data):
        #https_sslv3_handler = urllib.request.HTTPSHandler(context=ssl.SSLContext(ssl.PROTOCOL_SSLv3))##ssl https方式
        #opener = urllib.request.build_opener(https_sslv3_handler)
        #urllib.request.install_opener(opener)
        data = json.dumps(eval(data))
        data = data.encode('utf-8')
        url = 'http://' + self.host + ':' + str(self.port)  + url
        try:
            request = urllib.request.Request(url, headers=self.headers)
            response = urllib.request.urlopen(request, data)
            response = response.read().decode('utf-8')
            json_response = json.loads(response)
            return json_response
        except Exception as e:
            print('%s' % e)
            return {}

    def httpspost(self, url, params):
        https_sslv3_handler = urllib.request.HTTPSHandler(context=ssl.SSLContext(ssl.PROTOCOL_SSLv3))##ssl https方式
        opener = urllib.request.build_opener(https_sslv3_handler)
        urllib.request.install_opener(opener)
        
        data = json.dumps(eval(params))
        data = data.encode('utf-8')
        url = 'https://' + self.host + ':' + str(self.port)  + url
        
        try:
            request = urllib.request.Request(url, headers=self.headers)
            response = urllib.request.urlopen(request, data)
            response = response.read().decode('utf-8')
            json_response = json.loads(response)
            return json_response
        except Exception as e:
            print('%s' % e)
            return {}


       


    # 封装HTTP xxx请求方法
    # 自由扩展
