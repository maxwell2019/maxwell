# -*- coding:utf-8 -*-

__author__ = 'fubo'

import configparser
import mysql.connector
import sys

class GetDB:
    def __init__(self, ini_file, db):
        config = configparser.ConfigParser()

        config.read(ini_file)
        self.host = config[db]['host']
        self.port = config[db]['port']
        self.user = config[db]['user']
        self.passwd = config[db]['passwd']
        self.db = config[db]['db']
        self.charset = config[db]['charset']
        #self.sshhost = config[db]['sshhost']
        #self.sshuser = config[db]['sshuser']
        #self.sshpasswd = config[db]['sshpasswd']
        
    def get_conn(self):
        try:   
            conn = mysql.connector.connect(host=self.host, port=self.port, user=self.user, password=self.passwd, database=self.db, charset=self.charset)
            return conn
        except Exception as e:
            print('%s', e)
            sys.exit()

