# -*- coding:utf-8 -*-
from distutils.core import setup
import py2exe
 

setup(console=["main.py", "globalconfig.py", "globalconfig.py", "confighttp.py", "datastruct.py", "configrunmode.py", 
				"getdb.py", "runcase.py",  "test_interface_case.py","pyh.py","htmlreport.py"],
     data_files=[
#		("img",
#                  [r"d:\documents\matrix.jpg",
#                   r"d:\documents\Batman.jpg"]),
#
#                 ("xml",
#
#                  [r"D:\tmp\mypaint.xml",

#                   r"D:\tmp\mypuzzle.xml",

#                   r"D:\tmp\mypuzzle1.xml"])])
				("ini",

                  [r"D:\HttpTestproject\db_config.ini",

                   r"D:\HttpTestproject\http_conf.ini",

                   r"D:\HttpTestproject\run_case_config.ini"])])
