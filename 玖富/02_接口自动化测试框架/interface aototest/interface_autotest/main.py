# -*- coding:utf-8 -*-

__author__ = 'fubo'

import datetime
import unittest

from runcase import RunCase
from globalconfig import Global
from htmlreport import HtmlReport

if __name__ == '__main__':
    # 
    start_time = datetime.datetime.now()
    #
    global_config = Global()
    run_mode = global_config.get_run_mode() # 
    run_case_list = global_config.get_run_case_list()  # 
    db1_conn = global_config.get_db1_conn()   # 
    #db2_conn = global_config.get_db2_conn()   # 
    http = global_config.get_http()           # 
    
    # 
    runner = unittest.TextTestRunner()
    case_runner = RunCase()
    #case_runner.run_case(runner, run_mode, run_case_list, db1_conn, db2_conn, http)
    case_runner.run_case(runner, run_mode, run_case_list, db1_conn, http)#

    # 
    end_time = datetime.datetime.now()

    # 
    html_report = HtmlReport(db1_conn.cursor())
    
    html_report.set_time_took(str(end_time - start_time))  # 计算测试时间

    #
    html_report.generate_html('接口自动化测试报告_贷我飞', '../report.html')

    # # 
    global_config.clear()
