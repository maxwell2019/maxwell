# -*- coding:utf-8 -*-

__author__ = 'fubo'

import  unittest
# 
class ParametrizedTestCase(unittest.TestCase):

    #def __init__(self, methodName='runTest', test_wdata=None, http=None, db1_cursor=None, db2_cursor=None):
    def __init__(self, methodName='runTest', test_data=None, http=None, db1_cursor=None):#去掉db2验证
        super(ParametrizedTestCase, self).__init__(methodName)
        self.test_data = test_data
        self.http = http
        self.db1_cursor = db1_cursor
        #self.db2_cursor = db2_cursor

class TestInterfaceCase(ParametrizedTestCase):
    def setUp(self):
       pass

#
#    def getresponse_get():
#        response = self.http.get(self.test_data.request_url, self.test_data.request_param)
#        sReal = self.http.get(response)
#        return sReal.read()
   
    # 
    def test_get(self):
        
  # 
        self.db1_cursor.execute('SELECT test_expect FROM test_data WHERE case_id = %s', (self.test_data.case_id,))
        temp_result = self.db1_cursor.fetchone()
        self.sExpect = int(temp_result[0])

  #
        response = self.http.test_httpspost(self.test_data.request_url, self.test_data.request_param)
        self.sReal=response['dataReturn']['code']##获取返回的code
        self.test_data.reason = response['dataReturn']['message']
        self.test_data.test_desc = self.sReal
        
        if {} == response:
            self.test_data.result = 'Error'
            try:
                # 
                self.db1_cursor.execute('UPDATE test_result SET result = %s WHERE case_id = %s', (self.test_data.result, self.test_data.case_id))
                self.db1_cursor.execute('UPDATE test_result SET reason = %s WHERE case_id = %s', (self.test_data.reason, self.test_data.case_id))
                self.db1_cursor.execute('commit')
            except Exception as e:
                print('%s' % e)
                self.db1.cursor.execute('rollback')
            return
            
#
        if  self.sExpect == self.sReal:
            
            self.test_data.result = 'Pass'
            try:
                self.db1_cursor.execute('UPDATE test_result SET result = %s WHERE case_id = %s', (self.test_data.result, self.test_data.case_id))
                self.db1_cursor.execute('UPDATE test_result SET reason = %s WHERE case_id = %s', (self.test_data.reason, self.test_data.case_id))
                self.db1_cursor.execute('UPDATE test_result SET test_desc = %s WHERE case_id = %s', (self.test_data.test_desc, self.test_data.case_id))
                self.db1_cursor.execute('commit')
            except Exception as e:
                print('%s' % e)
                self.db1_cursor.execute('rollback')
            return
        else:
            self.test_data.result = 'Fail'
            try:
                self.db1_cursor.execute('UPDATE test_result SET result = %s WHERE case_id = %s', (self.test_data.result, self.test_data.case_id))
                self.db1_cursor.execute('UPDATE test_result SET reason = %s WHERE case_id = %s', (self.test_data.reason, self.test_data.case_id))
                self.db1_cursor.execute('UPDATE test_result SET test_desc = %s WHERE case_id = %s', (self.test_data.test_desc, self.test_data.case_id))
                self.db1_cursor.execute('commit')
            except Exception as e:
                print('%s' % e)
                self.db1_cursor.execute('rollback')
            return

        
# 
    def test_get1(self):

  # 
        self.db1_cursor.execute('SELECT test_expect FROM test_data WHERE case_id = %s', (self.test_data.case_id,))
        temp_result = self.db1_cursor.fetchone()
        self.sExpect = temp_result[0]
        
        response = self.http.get(self.test_data.request_url, self.test_data.request_param)
        self.sReal=response['dataReturn']['message']##
        
        if {} == response:
            self.test_data.result = 'Error'
            try:
                # 
                self.db1_cursor.execute('UPDATE test_result SET result = %s WHERE case_id = %s', (self.test_data.result, self.test_data.case_id))
                self.db1_cursor.execute('commit')
            except Exception as e:
                print('%s' % e)
                self.db1.cursor.execute('rollback')
            return
            
#
        if  self.sExpect == self.sReal:
            
            self.test_data.result = 'Pass'
            self.test_data.reason = self.sReal
            try:
                self.db1_cursor.execute('UPDATE test_result SET result = %s WHERE case_id = %s', (self.test_data.result, self.test_data.case_id))
                self.db1_cursor.execute('UPDATE test_result SET reason = %s WHERE case_id = %s', (self.test_data.reason, self.test_data.case_id))
                self.db1_cursor.execute('commit')
            except Exception as e:
                print('%s' % e)
                self.db1_cursor.execute('rollback')
            return
        else:
            self.test_data.result = 'Fail'
            self.test_data.reason = self.sReal
            try:
                self.db1_cursor.execute('UPDATE test_result SET result = %s WHERE case_id = %s', (self.test_data.result, self.test_data.case_id))
                self.db1_cursor.execute('UPDATE test_result SET reason = %s WHERE case_id = %s', (self.test_data.reason, self.test_data.case_id))
                self.db1_cursor.execute('commit')
            except Exception as e:
                print('%s' % e)
                self.db1_cursor.execute('rollback')
            return
        
        
# 
    def test_post(self):
   
  # 
        self.db1_cursor.execute('SELECT test_expect FROM test_data WHERE case_id = %s', (self.test_data.case_id,))
        temp_result = self.db1_cursor.fetchone()
        self.sExpect = temp_result[0]
        response = self.http.post(self.test_data.request_url, self.test_data.request_param)
        self.sReal=response['ip_msg']##

        if {} == response:
            self.test_data.result = 'Error'
            try:
                # 
                self.db1_cursor.execute('UPDATE test_result SET result = %s WHERE case_id = %s', (self.test_data.result, self.test_data.case_id))
                self.db1_cursor.execute('commit')
            except Exception as e:
                print('%s' % e)
                self.db1.cursor.execute('rollback')
            return
            
#
        if  self.sExpect == self.sReal:
            
            self.test_data.result = 'Pass'
            self.test_data.reason = self.sReal
            try:
                self.db1_cursor.execute('UPDATE test_result SET result = %s WHERE case_id = %s', (self.test_data.result, self.test_data.case_id))
                self.db1_cursor.execute('UPDATE test_result SET reason = %s WHERE case_id = %s', (self.test_data.reason, self.test_data.case_id))
                self.db1_cursor.execute('commit')
            except Exception as e:
                print('%s' % e)
                self.db1_cursor.execute('rollback')
            return
        else:
            self.test_data.result = 'Fail'
            self.test_data.reason = self.sReal
            try:
                self.db1_cursor.execute('UPDATE test_result SET result = %s WHERE case_id = %s', (self.test_data.result, self.test_data.case_id))
                self.db1_cursor.execute('UPDATE test_result SET reason = %s WHERE case_id = %s', (self.test_data.reason, self.test_data.case_id))
                self.db1_cursor.execute('commit')
            except Exception as e:
                print('%s' % e)
                self.db1_cursor.execute('rollback')
            return
        
# 
    def test_httpspost(self):
     

  # 
        self.db1_cursor.execute('SELECT test_expect FROM test_data WHERE case_id = %s', (self.test_data.case_id,))
        temp_result = self.db1_cursor.fetchone()
        self.sExpect = temp_result[0]

        response = self.http.test_httpspost(self.test_data.request_url, self.test_data.request_param)
        self.sReal=response['code']##
        
        if {} == response:
            self.test_data.result = 'Error'
            try:
                # 
                self.db1_cursor.execute('UPDATE test_result SET result = %s WHERE case_id = %s', (self.test_data.result, self.test_data.case_id))
                self.db1_cursor.execute('commit')
            except Exception as e:
                print('%s' % e)
                self.db1.cursor.execute('rollback')
            return
            
#
        if  self.sExpect == self.sReal:
            
            self.test_data.result = 'Pass'
            self.test_data.reason = self.sReal
            try:
                self.db1_cursor.execute('UPDATE test_result SET result = %s WHERE case_id = %s', (self.test_data.result, self.test_data.case_id))
                self.db1_cursor.execute('UPDATE test_result SET reason = %s WHERE case_id = %s', (self.test_data.reason, self.test_data.case_id))
                self.db1_cursor.execute('commit')
            except Exception as e:
                print('%s' % e)
                self.db1_cursor.execute('rollback')
            return
        
        else:
            self.test_data.result = 'Fail'
            self.test_data.reason = self.sReal
            try:
                self.db1_cursor.execute('UPDATE test_result SET result = %s WHERE case_id = %s', (self.test_data.result, self.test_data.case_id))
                self.db1_cursor.execute('UPDATE test_result SET reason = %s WHERE case_id = %s', (self.test_data.reason, self.test_data.case_id))
                self.db1_cursor.execute('commit')
            except Exception as e:
                print('%s' % e)
                self.db1_cursor.execute('rollback')
            return 

# 
    def test_httpscode(self):
    #
        response = self.http.test_httpspost(self.test_data.request_url, self.test_data.request_param)
        self.sReal = response['code']##
        self.test_data.test_desc = self.sReal
        self.test_data.reason = response['message']

   # 
        self.db1_cursor.execute('SELECT test_expect FROM test_data WHERE case_id = %s', (self.test_data.case_id,))
        temp_result = self.db1_cursor.fetchone()
        self.sExpect = int(temp_result[0])
        
        if {} == response:
            self.test_data.result = 'Error'
            try:
                # 
                self.db1_cursor.execute('UPDATE test_result SET result = %s WHERE case_id = %s', (self.test_data.result, self.test_data.case_id))
                self.db1_cursor.execute('UPDATE test_result SET reason = %s WHERE case_id = %s', (self.test_data.reason, self.test_data.case_id))#
                self.db1_cursor.execute('commit')
            except Exception as e:
                print('%s' % e)
                self.db1.cursor.execute('rollback')
            return{}
            
    #
        if  self.sReal == self.sExpect:
            self.test_data.result = 'Pass'
            
            try:
                self.db1_cursor.execute('UPDATE test_result SET result = %s WHERE case_id = %s', (self.test_data.result, self.test_data.case_id))
                self.db1_cursor.execute('UPDATE test_result SET reason = %s WHERE case_id = %s', (self.test_data.reason, self.test_data.case_id))
                self.db1_cursor.execute('UPDATE test_result SET test_desc = %s WHERE case_id = %s', (self.test_data.test_desc, self.test_data.case_id))

                self.db1_cursor.execute('commit')
            except Exception as e:
                print('%s' % e) 
                self.db1_cursor.execute('rollback')
            return{}
        
        else:
            self.test_data.result = 'Fail'
            
            try:
                self.db1_cursor.execute('UPDATE test_result SET result = %s WHERE case_id = %s', (self.test_data.result, self.test_data.case_id))
                self.db1_cursor.execute('UPDATE test_result SET reason = %s WHERE case_id = %s', (self.test_data.reason, self.test_data.case_id))
                self.db1_cursor.execute('UPDATE test_result SET test_desc = %s WHERE case_id = %s', (self.test_data.test_desc, self.test_data.case_id))
                self.db1_cursor.execute('commit')
            except Exception as e:
                print('%s' % e)
                self.db1_cursor.execute('rollback')
            return{}
        
def tearDown(self):
       pass
