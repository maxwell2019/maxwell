# -*- coding:utf-8 -*-
__author__ = '付波'

import configparser

class Config:
    def __init__(self,config_file,con):

        config = configparser.ConfigParser()
        config.read(config_file)

        try:
            self.project_info = config [con]["project"]
            print(self.project_info)
            self.problem_info = config[con]["problem"]
            print(self.problem_info)
            self.search_mode = config[con]["search_mode"]
            print(self.search_mode)

        except Exception as e:
            print('%s', e)

    def get_project_info(self):
        return self.project_info

    def get_problem_info(self):
        return self.problem_info

    def get_search_mode(self):
        return self.search_mode