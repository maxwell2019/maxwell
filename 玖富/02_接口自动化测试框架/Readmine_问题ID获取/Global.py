# -*- coding:utf-8 -*-

__author__ = 'fubo'

from config_info import Config

class Global:
    def __init__(self):
        self.config = Config('config_info.ini','CONFIG')

    def get_project_info(self):
        return self.config.get_project_info()
    #
    def get_problem_info(self):
        return self.config.get_problem_info()
    #
    def get_search_mode(self):
        return self.config.get_search_mode()