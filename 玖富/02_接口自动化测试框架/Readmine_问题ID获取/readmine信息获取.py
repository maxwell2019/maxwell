# coding:utf-8

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import time,csv,os
from Global import Global

def __init__(self):
    pass

def get_reamineinfo(self,project_info,problem_info,search_mode):
        fp = webdriver.FirefoxProfile()
        fp.set_preference("browser.download.folderList", 2)
        fp.set_preference("browser.download.dir", "E:\\9f")
        fp.set_preference("browser.download.manager.showWhenStarting", False)
        fp.set_preference("browser.helperApps.neverAsk.saveToDisk",
                          "application/octet-stream, application/vnd.ms-excel, text/csv, application/zip")
        Driver=webdriver.Firefox(firefox_profile= fp)
        Driver.get("http://192.168.150.28:81/redmine/login?back_url=http%3A%2F%2F192.168.150.28%3A81%2Fredmine%2F")
        print("输入用户名和密码")

        Driver.find_element_by_id("username").clear()
        Driver.find_element_by_id("username").send_keys("admin")
        Driver.implicitly_wait(20)
        Driver.find_element_by_id("password").clear()
        Driver.find_element_by_id("password").send_keys("adminfj")
        Driver.implicitly_wait(20)
        print("点击登录按钮")

        Driver.find_element_by_name("login").click()
        #选择项目
        Driver.find_element_by_xpath("/html/body/div/div[2]/div[1]/div[1]/ul/li[3]/a").click()
        Driver.implicitly_wait(20)

        #选择某个指定的项目 (第一步 )
        temp = Driver.find_elements_by_css_selector(".root")
        pj_temp = len(temp)
        for j in range (1,pj_temp):

            path_temp = '/html/body/div/div[2]/div[1]/div[3]/div[2]/div[2]/ul/li[%s]/div/a' %j
            print(path_temp)
            project_info_temp = Driver.find_element_by_xpath(path_temp).text

            if str(project_info_temp) == str(project_info):
                Driver.find_element_by_xpath(path_temp).click()
                Driver.implicitly_wait(20)
                break
            else:
                continue

        #选择问题类型
        pb_temp = 5 ##固定
        print(pb_temp)
        for j in range (1,pb_temp):
            path_temp = '/html/body/div/div[2]/div[1]/div[3]/div[2]/div[2]/div/table/tbody/tr[%s]/td[1]/a' %j
            print(path_temp)
            try:
                pb_info_temp = Driver.find_element_by_xpath(path_temp).text
            except Exception as e:
                print(e)
            print(pb_info_temp,problem_info)
            if str(pb_info_temp) == problem_info:
                Driver.find_element_by_xpath(path_temp).click()
                Driver.implicitly_wait(20)
                break
            else:
                continue

        #选择设定的条件
        temp = Driver.find_elements_by_css_selector(".queries>li")
        sm_temp = len(temp) + 1
        print(sm_temp)
        for j in range(1, sm_temp):
            path_temp = '/html/body/div/div[2]/div[1]/div[3]/div[1]/ul[2]/li[%s]/a' % j
            print(path_temp)
            search_mode_temp = Driver.find_element_by_xpath(path_temp).text
            print(search_mode_temp,search_mode)
            if str(search_mode_temp) == str(search_mode):
                Driver.find_element_by_xpath(path_temp).click()
                Driver.implicitly_wait(20)
                break
            else:
                continue

        # 获取问题数量
        try:
            temp = Driver.find_element_by_class_name("items").text ##获取次数
            print(temp)
            Driver.implicitly_wait(10)
            a = True

        except Exception as e:
            print(e)
            a = False

        if a== True:
            ##获取版本总数
            text = Driver.find_element_by_xpath(".//*[@id='content']/span/span/span[1]").text
            print(text)
            total = int(text.split("/")[1].split(")")[0])
            if total > 25:
                Driver.find_element_by_xpath(".//*[@id='content']/span/span/span[2]/span").click()  ##每页显示25
                print('每页25条记录显示')
                print('条目：%s' % total)
                num_temp = total / 25  ##计算出页数
                divmod_temp = total % 25  ##计算余数
                num = int(num_temp)
                if divmod_temp > 0:
                    num = num + 1
                else:
                    pass
            else:
                num = 1
            #print(team)
            print(num)
            id_list = []

            if num <= 1:
                team = Driver.find_elements_by_css_selector(".list.issues.sort-by-id.sort-desc tr")
                print('获取的number...')
                for line in team:
                    aa= line.get_attribute("id")
                    bc= aa[6:11]
                    print(bc)
                    id_list.append(bc)
            else:
                team = Driver.find_elements_by_css_selector(".list.issues.sort-by-id.sort-desc tr")
                print('获取第1页的number...')
                for line in team:
                    aa= line.get_attribute("id")
                    bc= aa[6:11]
                    print(bc)
                    id_list.append(bc)

                for i in range(1,num):
                    page = Driver.find_elements_by_css_selector(".pages li")
                    print('点击下一页')
                    shuliang = len(page)
                    path = '/html/body/div/div[2]/div[1]/div[3]/div[2]/span/ul/li[%s]/a' %shuliang
                    Driver.find_element_by_xpath(path).click()
                    Driver.implicitly_wait(20)
                    team = Driver.find_elements_by_css_selector(".list.issues.sort-by-id.sort-desc tr")
                    for line in team:
                        aa= line.get_attribute("id")
                        bc= aa[6:11]
                        print(bc)
                        id_list.append(bc)
                print(id_list)
        else:
            print('没有版本发布信息')
            id_list = []
        Driver.close()

        return id_list

def openfile(self,id_list):  # 读写到txt文件
    f = open("output_redmineinfo.txt", 'w')
    for i in  id_list:
        if i != '':
            f.write(str(i)+ '\r\n')
        else:
            continue
    f.close()

if __name__ == "__main__":
    config = Global()
    project_info = config.get_project_info()
    problem_info = config.get_problem_info()
    search_mode = config.get_search_mode()

    print(problem_info,project_info,search_mode)
    self = ''
    id_list = get_reamineinfo(self,project_info,problem_info,search_mode)
    openfile(self,id_list)