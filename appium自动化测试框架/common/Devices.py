# -*- coding: utf-8 -*-
# @File : Devices.py
# @Author : Maxwell
# @Time : 2019/10/14  18:46
# @Site : 
# @Software: PyCharm
# @QQ:7734374
# @Webchat :7734374
# @E-mail :7734374@qq.com
# @来源：TOPTesting软件测试网
'''
1、Devices 和sever, driver 是一对一的关系，有几个devices,就要起几个server, driver
2、Build 和 driver, device 是一对多的关系, 一个build 和一个device 组合成一个driver, 可以跑在多台devices上。
3、Server  和 driver 是一对一关系，通过port, bp来映射关系和通行。
4、Device 和 case是多对多关系，为了简单，我们把case放suite 里面，组成一对一关系。

'''
def get_devices_version(device):
    cmd = "adb -s {} shell getprop ro.build.version.release".format(device)
    result = run_command_on_shell(cmd)[0]
    return result


def get_devices_name(device):
    cmd = "adb -s {} shell getprop ro.product.model".format(device)
    result = run_command_on_shell(cmd)[0]
    return result


def list_devices():
    cmd = "adb devices"
    result = run_command_on_shell(cmd)
    print(result)
    return result


def get_devices_info():
    current = list_devices()
    devices = []
    j = 0
    port = 4723
    bootstrap = 5000
    for i in current[1:]:
        if i != "":
            each_device = {}
            nPos = i.index("\t")
            dev = i[:nPos]
            each_device["id"] = dev
            each_device["version"] = get_devices_version(dev)
            each_device["name"] = get_devices_name(dev)
            each_device["port"] = port + j
            each_device["bootstrap"] = bootstrap + j
            each_device["username"] = YAML().get_users()[j]
            devices.append(each_device)
            j = j + 1
    return devices
