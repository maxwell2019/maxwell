# -*- coding: utf-8 -*-
# @File : server.py
# @Author : Maxwell
# @Time : 2019/10/14  18:47
# @Site : 
# @Software: PyCharm
# @QQ:7734374
# @Webchat :7734374
# @E-mail :7734374@qq.com
# @来源：TOPTesting软件测试网
CMD = 'appium -a {} -p {} --bootstrap-port {} --session-override --command-timeout 600 -U {} >{} '


def close_appium_server():
    kill_progress_by_name("node")


def start_appium_server(device):
    host = "0.0.0.0"
    port = device['port']
    bootstrap_port = device['bootstrap']
    udid = device['id']
    appium_log = log_dir + "/" + "server.log"

    cmd = CMD.format(host, port, bootstrap_port, udid, appium_log)
    run_command_on_shell(cmd)