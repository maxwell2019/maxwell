# -*- coding: utf-8 -*-
# @File : myunit.py
# @Author : Maxwell
# @Time : 2019/10/14  18:44
# @Site : 
# @Software: PyCharm
# @QQ:7734374
# @Webchat :7734374
# @E-mail :7734374@qq.com
# @来源：TOPTesting软件测试网
import unittest
from common.desire_caps import *
from time import sleep


class StartEnd(unittest.TestCase):
    def setUp(self):
        logging.info('===setup====')
        self.driver = appium_desired()

    def tearDown(self):
        logging.info('====tearDown====')
        sleep(5)
        self.driver.close_app()