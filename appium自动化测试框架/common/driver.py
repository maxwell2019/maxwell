# -*- coding: utf-8 -*-
# @File : driver.py
# @Author : Maxwell
# @Time : 2019/10/14  18:48
# @Site : 
# @Software: PyCharm
# @QQ:7734374
# @Webchat :7734374
# @E-mail :7734374@qq.com
# @来源：TOPTesting软件测试网
def Base(device):
    capabilities = YAML().get_appium_config()

    if PLATFORM == 'Android':
        capabilities['app'] = AppPath.get_app_filename(build_path)
        capabilities['platformVersion'] = device["version"]
        capabilities['deviceName'] = device["name"]
        capabilities['udid'] = device["id"]
        driver = webdriver.Remote('http://localhost:{}/wd/hub'.format(device['port']), capabilities）