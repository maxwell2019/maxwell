# -*- coding: utf-8 -*-
# @File : desire_caps.py
# @Author : Maxwell
# @Time : 2019/10/14  18:42
# @Site : 
# @Software: PyCharm
# @QQ:7734374
# @Webchat :7734374
# @E-mail :7734374@qq.com
# @来源：TOPTesting软件测试网
import json
from appium import webdriver
import os
import logging.config

CON_LOG = r'../config/log.conf'
logging.config.fileConfig(CON_LOG)
logging = logging.getLogger()

def appium_desired():

    with open("../config/desire_caps", "r") as file:
        data = json.load(file)

    desired_caps = {}
    # print(data['platformName'])
    desired_caps['platformName'] = data['platformName']
    desired_caps['deviceName'] = data['deviceName']
    desired_caps['deviceVersion'] = data['deviceVersion']

    BASE_DIR = os.path.dirname(os.path.dirname(__file__))
    App_Path = os.path.join(BASE_DIR, 'app', data['appName'])
    desired_caps['app'] = App_Path

    desired_caps['noReset'] = data['noReset']

    desired_caps['unicodeKeyboard'] = data['unicodeKeyboard']
    desired_caps['resetKeyboard'] = data['resetKeyboard']

    desired_caps['appPackage'] = data['appPackage']
    desired_caps['appActivity'] = data['appActivity']

    logging.info('start run app...')
    driver = webdriver.Remote("http://%s:%s/wd/hub" % (data['host'], data['port']), desired_caps)

    driver.implicitly_wait(5)
    return driver

if __name__ == '__main__':
    appium_desired()