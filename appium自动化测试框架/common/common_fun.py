# -*- coding: utf-8 -*-
# @File : common_fun.py
# @Author : Maxwell
# @Time : 2019/10/14  18:43
# @Site : 
# @Software: PyCharm
# @QQ:7734374
# @Webchat :7734374
# @E-mail :7734374@qq.com
# @来源：TOPTesting软件测试网
from baseViews.baseView import BaseView
from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException
from common.desire_caps import *
import time
import os
import csv


# 完成公用的类及其方法
class Common(BaseView):
    def set_Btn(self, match, value):
        Btn = (match, value)
        return Btn

    def check_updateBtn(self):
        logging.info("===========check updateBtn=======")
        btn_name = 'android:id/button2'
        update_btn = self.set_Btn(By.ID, btn_name)
        try:
            element = self.find_element(update_btn[0], update_btn[1])
        except NoSuchElementException:
            logging.info("===not found update button====")
        else:
            logging.info("===check update button====")
            element.click()

    def check_skipBtn(self):
        logging.info('====check skipBtn====')
        btn_name = 'com.tal.kaoyan:id/tv_skip'
        btn = self.set_Btn(By.ID, btn_name)
        try:
            element = self.find_element(btn[0], btn[1])
        except NoSuchElementException:
            logging.info("===not found skip button====")
        else:
            logging.info("===check skip button====")
            element.click()

    def get_screenSize(self):
        '''
        获取屏幕尺寸
        :return:
        '''
        x = self.get_window_size()['width']
        y = self.get_window_size()['height']
        return (x, y)

    def swipeLeft(self):
        logging.info('swipeLeft')
        l = self.get_screenSize()
        y1 = int(l[1] * 0.5)
        x1 = int(l[0] * 0.95)
        x2 = int(l[0] * 0.25)
        self.swipe(x1, y1, x2, y1, 1000)

    def getTime(self):
        self.now = time.strftime("%Y-%m-%d %H_%M_%S")
        return self.now

    def getScreenShot(self, module):
        time = self.getTime()
        image_file = os.path.dirname(os.path.dirname(__file__)) + '/screenshots/%s_%s.png' % (module, time)

        logging.info('get %s screenshot' % module)
        self.driver.get_screenshot_as_file(image_file)

    def check_market_ad(self):
        '''检测登录或者注册之后的界面浮窗广告'''
        logging.info('=======check_market_ad=============')
        self.wemedia = self.set_Btn(By.ID, 'com.tal.kaoyan:id/view_wemedia_cacel')
        try:
            element = self.driver.find_element(*self.wemedia)
        except NoSuchElementException:
            pass
        else:
            logging.info('close market ad')
            element.click()

    def get_csv_data(self, csv_file, line):
        '''
        获取csv文件指定行的数据
        :param csv_file: csv文件路径
        :param line: 数据行数
        :return:
        '''
        BASE_DIR = os.path.dirname(os.path.dirname(__file__))
        csv_file_path = os.path.join(BASE_DIR, 'data', csv_file)
        with open(csv_file_path, 'r', encoding='utf-8-sig') as file:
            reader = csv.reader(file)
            for index, row in enumerate(reader, 1):
                if index == line:
                    return row


if __name__ == '__main__':
    driver = appium_desired()
    com = Common(driver)
    com.check_updateBtn()
    com.check_skipBtn()
    com.swipeLeft()
    com.swipeLeft()
    com.getScreenShot('startapp')